# mikaku

Bonjour et bienvenue sur la v4 de mikaku. Pour cette 4e version, le but est que le site soit statique mais plus dynamique. Hébergé sur gitlab

Ce site utilise obsidian et quartz

Dans le dossier `content` se trouve la _base de donnée_, sous format markdown
Le dossier `public` est le dossier qui sera publié statiquement
Le dossier `quartz/styles` contient les scss

La police de caractère du titre est _gloria hallelujah_