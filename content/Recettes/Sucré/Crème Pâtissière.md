---
date: 2023-11-14T21:00

image:
---
# Introduction
---

La base du Paris-Brest.
À proportion égale, Mettre 25% de crème de plus que le lait dans la pâte à choux

| Sujet                | Donnée         |
| -------------------- | -------------- |
| Par                  | Tonton mimi    |
| Pour                 | Un Paris-Brest |
| Temps de préparation | 25m            |
| Durée de cuisson     | 10m            |
| Temps total          | 35m            |

# Ingrédients
---
- 110g de [[sucre]]
- 65cl de [[lait]]
- 110g de [[poudre crème pâtissière Impérial]]
- 7 [[œufs]]
- 5g de [[beurre]]

# Instructions
---
1. Mélanger 110g de sucre et 65cl de lait
2. Mélanger 110g de poudre crème pâtissière impérial avec 10cl de lait et mélanger avec un fouet pour que ce soit juste liquide (vous pouvez rajouter l'œuf restant de la recette la pâte à choux)
3. Faire bouillir le lait
4. Une fois que ça bout, mélanger la crême et le lait, mettre le lait dans la poudre diluée
5. Repasser un petit coup sur le feu pour la sécher un peu en remuant jusqu'à ce que ça fasse des glouglou
6. Mettre un peu de beurre sur la crème pâtissière pour éviter les croûtes
7. Laisser refroidir

#Dessert #Français #Sucré