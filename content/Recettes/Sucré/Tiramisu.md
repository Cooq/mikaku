---
date: 2024-02-05T14:56
image:
  - /images/tiramisu.webp
---
# Introduction
---

L'Italie au bout des lèvres

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 8 personnes |
| Temps de préparation | 20m         |
| Durée de cuisson     | 0           |
| Temps total          | 4h          |


# Ingrédients
---
- 3 [[œufs]]
- 500g [[mascarpone]]
- 100g [[sucre]]
- 15cl [[café]]
- 3 cuillères à soupe [[marsala]]
- 250g [[biscuit à la cuillère]]
- 2 cuillères à soupe [[cacao]]
- 100g copeaux de [[chocolat noir]]
- 3 feuilles de [[gélatine]]

# Instructions
---
1. Faites tremper la gélatine dans de l'eau froide et préparez un café
2. Séparer les blancs des jaunes
3. Mettez les jaunes et le sucre dans une jatte. Battez avec un fouet électrique jusqu'à ce que le mélange soit devenu bien blanc
4. Faites fondre la gélatine dans une poêle à feu doux
5. Ajoutez le mascarpone dans la jatte, cuillerée par cuillerée avec la gélatine
6. Battez les blancs en neige
7. Incorporez les blancs délicatement dans le mélange
8. Mélangez le café refroidi avec le marsala dans de la vaisselle plate
9. Passez rapidement les biscuits à la cuillère dans le café et tapissez les dans le fond d'un plat
10. Couvrez avec la moitié de la préparation à base de mascarpone
11. Déposez les copeaux chocolat sur la préparation et rajoutez le reste de la préparation
12. Laissez reposer au moins 4h
13. Saupoudrez de cacao avec une passoire

#Dessert #Italien #Sucré