---
date: 2024-02-05T14:02
image:
  - /images/canneles.webp
---
# Introduction
---

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | Maman  |
| Pour                 | ??     |
| Temps de préparation | 15m    |
| Durée de cuisson     | 55m    |
| Temps total          | 1h10   |


# Ingrédients
---
- 620 g [[lait]]
- 1 gousse de [[vanille]] Bourbon
- 310 g [[sucre]]
- 150 g [[farine]] T45
- 1 pincée [[sel]]
- 2 [[œufs]]
- 3 [[jaune d'œuf|jaunes d'œufs]]
- 50 g [[beurre]]
- 30 g [[rhum|Rhum ambré]]

# Instructions
---
1. Faites frémir le lait avec les graines de la gousse de vanille puis faites infuser. Laissez reposer 1h puis filtrez à travers une passoire
2. Mélangez au fouet le sucre, la farine et le sel
3. Ajoutez les œufs et les jaunes puis vers 1/3 du lait. Mélangez
4. Versez le beurre fondu, le reste du lait et le Rhum
5. Laissez reposer 12h à température ambiante
6. Le lendemain, préchauffez votre four à 240°C (th. 8) et placez vos empreintes sur la plaque perforée
7. Mélangez la préparation au fouet puis remplissez les empreintes à 5mm du bord
8. Faites cuire 20min à 240°C (th. 8) puis 35min à 180°C (th. 6). Démoulez dès la sortie du four.


#Dessert #Français #Sucré