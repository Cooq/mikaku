---
date: 2024-06-23T16:16
image:
---
# Introduction
---

Une crème anglaise rapide

| Sujet                | Donnée                 |
| -------------------- | ---------------------- |
| Par                  | Adam                   |
| Pour                 | 0.5L de crème anglaise |
| Temps de préparation | 10min                  |
| Durée de cuisson     | 5min                   |
| Temps total          | 15min                  |

# Ingrédients
---
- 62g de [[crème fraiche]] 35% MG
- 62g de [[lait]]
- 38g de [[jaune d'œuf]] (~2 œufs)
- 13g de [[sucre]]

# Instructions
---
1. Mélanger la crème et le lait puis faire chauffer
2. Mélanger le sucre et jaune
3. Au moment de l'ébullition, mettre un peu de lait dans les jaunes et touiller
4. Puis mettre les jaunes dans le lait hors du feu, mélanger puis remettre sur le feu en faisant des 8 avec une spatule en bois ou Marise
5. La crème est prête lorsqu'elle est à 81°C ou faire le [[#^2b0a3a|Test du doigt]]


> [!faq] Test du doigt
> 
> ![[schema-creme-anglaise.svg]]

^2b0a3a


#Sucré  #Dessert 