---
date: 2024-02-05T14:04
image:
  - /images/crepes.webp
---
# Introduction
---
Tout plat comme il faut

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Nicolas     |
| Pour                 | 4 personnes |
| Temps de préparation | 5m          |
| Durée de cuisson     | 1h          |
| Temps total          | 1h05        |


# Ingrédients
---
- 250 g [[farine]]
- 2 [[œufs]]
- 1 cuillère à soupe [[huile végétale]]
- 1 pincée sel
- 300 ml [[lait]]
- 300 ml eau

# Instructions
---
1. Mélanger le lait, l'eau, les œufs et la farine
2. Ajouter un pincée de sel
3. Battre le tout pour que ce soit bien homogène
4. Faire cuire

#Dessert #Français #Sucré