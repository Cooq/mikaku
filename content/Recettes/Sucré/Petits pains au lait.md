---
date: 2024-02-05T14:49
image:
---
# Introduction
---

Revu par Nicolas

| Sujet                | Donnée          |
| -------------------- | --------------- |
| Par                  | Nicolas         |
| Pour                 | 14 petits pains |
| Temps de préparation | 30m             |
| Durée de cuisson     | 15m             |
| Temps total          | 3h              |


# Ingrédients
---
- 500g [[farine]]
- 10g sel
- 40g [[sucre]]
- 25cl [[lait]]
- 100g [[beurre]]
- 1 [[œufs]]
- 20g [[levure de boulanger]] fraiche

# Instructions
---
1. Mélanger la farine, le sucre et le sel.
2. Faites ramollir le beurre, pour qu'il ait une texture de pommade. Le mieux est de le sortir la veille. Ne jamais le faire fondre au micro-onde, au pire le mettre au soleil pour qu'il ramollisse ou le battre avec un rouleau à pâtisserie.
3. Dans un bol séparé, mélanger le lait et l'œuf.
4. Ajouter la levure à la farine.
5. Mélanger la préparation du œuf/lait avec la farine.
6. Pétrir 3 min le temps d'avoir une pate homogène, elle doit totalement se décoller du bol.
7. Couper le beurre en petit cube et l'ajouter quand le mélange est homogène.
8. Pétrir 7 min.
9. Faire reposer la pâte dans un récipient en plastique jusqu'à ce qu'elle triple de volume. Le mieux est de la faire reposer soit à 40°C entre 1 à 2h pas plus soit 4h à température ambiante soit toute la nuit au frigo. Chacune de ces méthodes évitera que votre petit pain sente la levure.
10. Préchauffez le four à 180°C (Th 6) cuisson par le bas.
11. Faire des boules de poids égale (environ 70g), ne pas hésiter à mettre de la farine sur les mains pour ne pas que les boules collent, mais normalement vous n'en aurez pas besoin. Positionner les boules sur une plaque de cuisson et attendre qu'elles montent. Elles doivent doubler voire tripler de volume.
12. Une fois levée, dorer les boules avec du jaune d'œuf et ciselez les avec un couteau.
13. Enfourner dans le four chaud pendant 15 min.
14. Félicitations vous avez réussi vos petits pains.

#Petit-Déjeuner #Français #Sucré