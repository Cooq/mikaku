---
date: 2024-02-05T13:58
image:
  - /images/brioche.webp
---
# Introduction
---
« Qu'ils mangent de la brioche ! » - Marie-Antoinette (peut-être)

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Source               | Maman       |
| Portions             | 36 brioches |
| Temps de préparation | 1h          |
| Durée de cuisson     | 40m         |
| Temps total          | 1h40        |

# Ingrédients
---
- 10 cl eau
- 1 kg [[farine]]
- 2 pincées sel
- 200 g [[sucre]]
- 500 g [[beurre]]
- 12 [[œufs]]
- 2 sachets [[levure de boulanger]] sèche

# Instructions
---
1. Clarifier le beurre. Faire chauffer le beurre et ne récupérer que la partie jaune / claire (enlever le petit lait)
2. Placer la farine dans un récipient, le sucre et le sel. Bien mélanger
3. Dans un autre récipient, casser les œufs et les battre en omelette
4. Faire chauffer l'eau à 37°C, y ajouter une pincée de sucre et y diluer la levure. Attendre au minimum qu'elle double de volume
5. Mélanger la levure avec la farine
6. Incorporer les œufs à la pâte
7. Rajouter le beurre doucement par petite quantités. Cela doit faire une pâte homogène
8. Laisser reposer et attendre que la pâte double de volume (au moins 3/4h - 1h)
9. Moulez la pâte et attendre que la pâte lève à nouveau
10. Badigeonner de lait à l'aide d'un pinceau le dessus de la brioche
11. Pour des briochettes, placer dans un four à 160°C, 20min
12. Pour une grosse brioche placer dans un four à 160°C, environ 40min. Piquer régulièrement pour vérifier

#Petit-Déjeuner #Sucré #Français