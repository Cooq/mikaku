---
date: 2024-06-23T18:14
image:
  - /images/Mille-feuille.webp
---
# Introduction
---

En fait, il n'y en a pas 1000, mensonges !

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 6 personnes |
| Temps de préparation | 2h          |
| Durée de cuisson     | 30min       |
| Temps total          | 2h30        |

# Ingrédients
---
## Pâte feuilleté
- 100g de [[Ingrédients/pâte feuilleté|pâte feuilleté]] (ou [[Recettes/Salé/Pâte feuilleté|Pâte feuilleté maison]])
## Crème Pâtissière
- 500g de [[lait]]
- 84 de [[jaune d'œuf]] (~4,2 œuf)
- 150g de [[sucre]]
- 25g de [[poudre crème pâtissière Impérial|poudre à crème]]
- 25g de [[farine]]
- 1 gousse de vanille
## Fondant
- 10g d'eau
- 12,3g de [[sucre]]
- 20g de [[fondant]]
## Autres
- 5g de [[sirop de glucose]]
- 10g de cacao

# Instructions
---
## Pâte feuilleté
1. Faire 3 carrés de la taille de votre mille feuille (~ 10cm x 10cm)
2. Bien trouer la pâte pour qu'elle ne gonfle pas
3. L'enfourner à 180°C jusqu'à ce qu'elle soit cuite
*Pour qu'elle ne gonfle pas, vous pouvez mettre du papier cuisson et mettre une plaque par dessus*
## Crème Pâtissière
1. Mélanger les jaunes d'œufs, le sucre, la poudre à crème et la farine
2. Rajouter un peu de lait pour délier
3. Faire chauffer le reste de lait avec la vanille
4. Au moment de l'ébullition, ajouter un peu de lait chaud dans les œufs puis, hors du feu, mettez le mélange dans le lait
5. Remettre sur le feu et touiller jusqu'à obtenir une pâte épaisse
6. La crème est prête dès qu'elle bout lorsque le mouvement est arrêté
7. Déposer la crème sur une plaque cellophanée
8. Recouvrir de cellophane et mettre au frigo 30min
## Montage
1. Mettre la crème pâtissière dans un robot muni d'une feuille et battre jusqu'à obtenir un mélange un peu moins compact (mais pas liquide non plus, sinon il faut remettre au frigo)
2. Déposer une feuille de pâte feuilleté collée à un carton au sirop de glucose
3. Faire des traits de crème pâtissière à la poche à douille sur la pâte feuilletée jusqu'à ce qu'elle soit remplie
4. Remettre un bout de pâte feuilletée
5. Rajouter un peu de crème pâtissière sur les bords et égaliser à l'aide d'une spatule
6. Remettre de la crème pâtissière puis le dernier bout de pâte feuilleté puis égaliser encore une fois
## Fondant
1. Faire le sirop en mélangeant le sucre et l'eau, faire bouillir
2. Mélanger le sirop et le fondant
3. Mettre le fondant sur le gâteau tout en en gardant un petit peu 
4. Étaler à l'aide d'une spatule
5. Mélanger le cacao avec le reste de fondant
6. Faire un cornet avec du papier cuisson puis mettre le fondant noir dedans
7. Faire des traits très fin parallèles sur le fondant blanc
8. Pour faire la fin du décors, au couteau faire des traits dans un sens puis dans l'eau pour avoir un joli pattern
9. Nettoyer les bords avec le couteau
10. Mettre au frigo 1h, c'est prêt


#Français #Sucré #Dessert 