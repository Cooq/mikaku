---
date: 2024-06-23T15:47
image:
  - /images/Dacquoise.webp
---
# Introduction
---

La base du royal et de bien d'autres gâteaux

| Sujet                | Donnée    |
| -------------------- | --------- |
| Par                  | Adam      |
| Pour                 | 4 royales |
| Temps de préparation | 15min     |
| Durée de cuisson     | 10min     |
| Temps total          | 25min     |

# Ingrédients
---
- 150g de [[blanc d'œuf]] (~3 œufs)
- 50g de [[sucre]] semoule
- 110g de [[poudre d'amande]]
- 150g de [[sucre glace]]

# Instructions
---
1. Mettre les blancs dans un batteur à vitesse moyenne
2. Dès que les blancs commence à mousser un petit peu, ajouter le sucre petit à petit en augmentant la vitesse au fur et à mesure
3. Arrêter de battre quand les blancs font un petit pic (~2cm) au bout du fouet dès que vous le sortez
4. Mélanger et tamiser le sucre glace avec la poudre d'amandes
5. Incorporer le mélange aux œufs en faisant des mouvement rotatifs du bas vers le haut avec une Marise
6. Étaler le tout avec une spatule coudée sur une plaque recouverte de papier sulfurisé
7. Mettre au four à 180°C pendant 10min
8. Il faut que le gâteau soit à peine cuit, pour vérifier qu'il est cuit, toucher la surface du gâteau délicatement, si la trace de doigt remonte, le gâteau peut être sorti

#Base #Français #Sucré #Dessert 