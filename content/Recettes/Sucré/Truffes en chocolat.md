---
date: 2024-02-05T14:58
image:
  - /images/truffes.webp
---

# Introduction
---

Le top du top des douceurs chocolatées

| Sujet                | Donnée           |
| -------------------- | ---------------- |
| Par                  | Teresa           |
| Pour                 | plein de truffes |
| Temps de préparation | 2j               |
| Durée de cuisson     | 0                |
| Temps total          | 2j               |


# Ingrédients
---
- 500g [[lait]]
- 1.25Kg [[chocolat noir]]
- 15g [[miel]]
- 2Kg [[chocolat au lait]] ou noir (pour l'enrobage)
- 100g [[cacao]] amère en poudre

# Instructions
---
1. Faire fondre le chocolat au bain marie.
2. À part, faire chauffer le lait et le miel. Puis mélanger avec le chocolat.
3. Mouler dans une feuille de plastique sur une épaisseur d'1cm.
4. Faire des boules dès que le mélange durcit (environ 2-3h).
5. Laisser de nouveau durcir pour pouvoir les enrober.
6. Faire fondre le chocolat au bain marie pour l'enrobage.
7. Une fois fondue, sortir le chocolat du bain marie et enrober les boules en les plongeant dedans (laisser 2-3sec).
8. Les déposer sur le plan de travail recouvert de cacao amère et les rouler dedans à l'aide du dos d'une fourchette, une fois refroidie afin qu'elle s'entoure du pellicule de cacao.
9. Les laisser refroidir et durcir avant de les mettre au réfrigirateur pour stockage.

#Dessert #Français #Sucré