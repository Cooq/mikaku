---
date: 2023-11-14T21:00:00.000Z
image:
---
# Introduction
---

| Sujet                | Donnée         |
| -------------------- | -------------- |
| Par                  | Tonton mimi    |
| Pour                 | Un Paris-Brest |
| Temps de préparation | 25m            |
|Durée de cuisson | |
|Temps total| 25m |

La base du Paris-Brest

# Ingrédients
---
- [[Crème Pâtissière]]
- 375g de [[beurre]]
- 300g de [[praliné]]

# Instructions
---
1. Fouetter le beurre jusqu'à ce qu'il soit en pommade (Si le beurre est trop froid, chauffer le bol au sèche cheveux)
2. Mélanger pralin (remuer le pralin s'il s'est séparé) avec le beurre, cuillère par cuillère
3. Fouetter la crème pâtissière
4. L'incorporer cuillère par cuillère avec le pralin
5. Fouetter jusqu'à ce que ce soit ferme
6. Couper le chapeau du chou au ciseaux
7. Remplir le chou avec une douille cannelée (Pour éviter que la crème coule, enfoncer le sac dans la douille)
8. D'abord remplir le fond avec des mouvements gauches / droite
9. Faire les bords, déborder un peu. Finir par l'intérieur
10. Saupoudrer de sucre glace juste avant de servir

#Dessert #Français #Sucré