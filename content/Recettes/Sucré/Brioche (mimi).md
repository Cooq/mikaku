---
date: 2024-02-05T13:54
image:
---
# Introduction
---
Recette de boulanger


| Sujet                | Donnée      |
| -------------------- | ----------- |
| Source               | Mimi        |
| Portions             | 10 portions |
| Temps de préparation | 3m          |
| Durée de cuisson     | 20m         |
| Temps total          | 23m         |



# Ingrédients
---
- 250g [[farine]]
- 5g sel
- 40g [[sucre]]
- 75g [[beurre]]
- 3 [[œufs]]
- 30 g [[levure de boulanger]] fraiche

# Instructions
---
1. Mélanger la farine et le sel.
2. Faites ramolir le beurre, pour qu'il ait une texture de pommade. Le mieux est de le sortir la veille. Ne jamais le faire fondre au micro-onde, au pire le mettre au soleil pour qu'il ramolisse ou le battre avec un rouleau à patisserie.
3. Ajouter au mélange tous les autres ingrédients sauf le beurre.
4. Couper le beurre en petit cube et l'ajouter quand le mélange est homogène.
5. Pétrir pendant 15 min à basse vitesse et finir les 3 dernières minutes à vitesse élevée. La pâte doit se décoller du bol.
6. Faire reposer la pâte dans un récipient en plastique jusqu'à ce qu'elle triple de volume. Le mieux est de la faire reposer soit à 40°C entre 1 à 2h pas plus soit 4h à température ambiante soit toute la nuit au frigo. Chacune de ces méthodes évitera que votre brioche sente la levure.
7. Préchauffez le four à 180°C (Th 6).
8. Faire des boules de poids égale (environ 50g), ne pas hésiter à mettre de la farine sur les mains pour ne pas que les boules collent. Positionner les boules dans un moule et attendre qu'elles montent. Elles doivent doubler voire tripler de volume.
9. Une fois levée, dorer les boules avec du jaune d'oeuf ou du beurre liquide.
10. Enfourner dans le four chaud pendand 20 min.
11. Félicitations vous avez réussi votre brioche.

#Français #Petit-Déjeuner #Sucré