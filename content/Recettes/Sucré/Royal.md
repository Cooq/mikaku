---
date: 2024-06-23T15:43
image:
  - /images/Royal.webp
---
# Introduction
---

Aussi bon que chez le pâtissier

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 6 personnes |
| Temps de préparation | 1h          |
| Durée de cuisson     |             |
| Temps total          | 4h          |
# Ingrédients
---
## Dacquoise Amandes
![[Dacquoise Amandes#Ingrédients]]

## Feuilletine
![[Feuilletine#Ingrédients]]

## Mousse au chocolat
![[Mousse au chocolat (Royal)#Ingrédients]]

# Instructions
## Dacquoise Amandes
![[Dacquoise Amandes#Instructions]]

- Faire un rond de dacquoise avec un cercle (*Il est possible de faire un rond plus petit que le rond final pour que la dacquoise ne soit pas visible sur le gâteau final*)
## Feuilletine
![[Feuilletine#Instructions]]

- Tartiner la feuilletine sur la dacquoise sans dépasser et la mettre au congélateur avec le rond le temps de préparer la mousse au chocolat
## Mousse au chocolat
![[Mousse au chocolat (Royal)#Instructions]]

- Mettre la mousse au chocolat sur le gâteau (avec un cercle) puis mettre au congélateur 2-3h
- Garnir de fondant noir réchauffé délayé d'un sirop, racler avec une spatule pour avoir une finition lisse
*Pour ne pas avoir de bulles dans le fondant, vous pouvez utiliser un mixeur en bras*
- Remettre au frigo

#Français #Sucré  #Dessert 