---
date: 2023-03-13T09:50
image:
---
# Introduction
---
La base du Paris-Brest

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Tonton mimi |
| Pour                 | ???         |
| Temps de préparation | 25m         |
| Durée de cuisson     | 45m         |
| Temps total          | 1h10        |


# Ingrédients
---

- 0.5L de [[lait]] demi écrémé
- 250g de [[beurre]]
- 300g de [[farine]]
- 7 [[œufs]]

# Instructions
---

1. Mélanger le lait demi écrémé et le de beurre
2. Mettre à ébullition
3. Tamiser la farine
4. Mélanger la farine et le lait (commis recommandé)
5. Remettre sur le feu et remuer un peu pour enlever la vapeur d'eau
6. Dans un robot, mettre les œufs un par un. Attendre que l'œuf soit intégré avant de mettre le suivant pour ne pas qu'il cuise sur le côté
7. Faire les choux à la poche à douilles (Si vous faites un chiffre avec, posez en pyramide 3 traits puis 2 puis 1)
8. Mettre un œuf dans un verre, le fouetter à la fourchette, et l'appliquer au pinceau sur la pâte à choux
9. Faire des traits à la fourchette sur le long du boudin
10. Mettre des amandes effilées dessus
11. Faire cuire à 160°C 45min. Attention, ne jamais ouvrir le four pendant la cuisson

#Base #Français #Sucré
