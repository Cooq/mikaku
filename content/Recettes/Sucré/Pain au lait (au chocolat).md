---
date: 2024-02-05T14:48
image:
  - /images/paint_au_lait_au_chocolat.webp
---
# Introduction
---

Tiktok au chocolat

| Sujet                | Donnée           |
| -------------------- | ---------------- |
| Par                  | Nicolas          |
| Pour                 | 20 pains au lait |
| Temps de préparation | 1h50             |
| Durée de cuisson     | 15m              |
| Temps total          | 2h05             |


# Ingrédients
---
- 3 [[œufs]]
- 150g [[sucre]]
- 750g [[farine]]
- 1 sachet [[levure de boulanger]]
- 5g sel
- 250 ml lait tiède
- 150g [[beurre]]

# Instructions
---
1. Diluer la levure dans le lait tiède (environ 37°C)
2. Ajouter le sucre, les œufs et le beurre fondu. Mélanger
3. Ajouter la farine puis le sel. Mélanger
4. Pétrir 3 à 4min sur le plan de travail
5. Laisser reposer 1h
6. Couper des pâtons de 90g
7. Étaler chaque pâtons en longueur puis les rouler sur eux-mêmes
8. Optionnel : ajouter du chocolat avant de rouler. Ne pas mettre jusqu’au bord
9. Laisser reposer 20min
10. Badigeonner avec un mélange lait, jaune d’œuf
11. Enfourner 10 à 15min à 200°C

#Dessert #Français #Sucré
