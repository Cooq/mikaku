---
date: 2024-02-05T14:22
image:
  - /images/gaufres.webp
---
# Introduction
---
Pour changer des crêpes !

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 6 personnes |
| Temps de préparation | 5m          |
| Durée de cuisson     | 25m         |
| Temps total          | 30m         |


# Ingrédients
---
- 300g [[farine]]
- 1 pincée [[sel]]
- 75g [[sucre]]
- 500ml [[lait]]
- 2 [[œufs]]
- 1 sachet [[sucre vanillé]]
- 1 sachet [[levure chimique]]

# Instructions
---
1. Préchauffer l'appareil à gaufres
2. Mélanger le sel à la farine
3. Dans un mixeur, mixer le lait, les oeufs, le sucre et la levure
4. Rajouter la farine petit à petit pour éviter les grumeaux
5. Verser la préparation à la louche dans l'appareil chaud. Attendre 3 à 6min (suivant vos préférences), démouler et réitérer

#Dessert #Français #Sucré
