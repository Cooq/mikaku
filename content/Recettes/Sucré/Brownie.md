---
date: 2024-04-22T13:54
image:
---
# Introduction
---

Il est possible de faire ça en 3 méthodes différentes :
Mixer le beurre et le sucre puis incorporer le reste, le résultat sera le plus léger et le plus fort en chocolat
Mixer les œufs avec le sucre, le beurre, sucre avec le chocolat et mixer les deux ensemble puis finir par la farine. Résultat plus compact avec un gout chocolaté moins prononcé
Mixer tous les ingrédients liquides puis ajouter les ingrédients secs. Le résultat sera plus compact et shiny

| Sujet                | Donnée              |
| -------------------- | ------------------- |
| Par                  | Un site de recettes |
| Pour                 | 6 personnes         |
| Temps de préparation | 10m                 |
| Durée de cuisson     | 20m                 |
| Temps total          | 30m                 |


# Ingrédients
---
- 20g d'[[amandes en poudre]]
- 3 [[œufs]]
- 120g de [[sucre]]
- 130g de [[beurre]]
- 50g de [[farine]]
- 200g de [[chocolat noir]]
- 120g de cerneaux de [[noix]] grossièrement hachés
- 2 pincées de [[cannelle]]

# Instructions
---
1. Préchauffer le four à 180°C (thermostat 6).
2. Faire fondre le chocolat noir.
3. Dans un saladier, mélanger le sucre avec les œufs.
4. Incorporer le beurre ramolli, la farine, les amandes en poudre, le chocolat fondu, la cannelle et les noix.
5. Verser la pâte dans le moule.
6. Enfourner à 180°C et laisser cuire 20 à 25 minutes.

#Dessert #AmériqueDuNord #Sucré