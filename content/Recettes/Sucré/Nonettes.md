---
date: 2024-02-05T14:43
image:
  - /images/nonettes.webp
---
# Introduction
---

Comme des nones, en plus petites

| Sujet                | Donnée            |
| -------------------- | ----------------- |
| Par                  | Teresa            |
| Pour                 | plein de nonettes |
| Temps de préparation | 1h                |
| Durée de cuisson     | 20m               |
| Temps total          | 1h20              |


# Ingrédients
---
- 200g eau
- 200g [[miel]]
- 25g [[sucre]]
- 75g [[beurre]]
- zeste une [[orange]]
- 100g [[marmelade d'orange]]
- 1 [[étoile de badiane]]
- 275g [[farine]]
- 1 cuillère café d'[[épice à pain d'épice]]
- 1 capsule de [[cardamone]]
- 1 sachet [[levure chimique]]
- 30g [[sucre glace]]
- 1 cuillère à soupe [rhum](rhum.md)
- 1 cuillère à café [[bicarbonate de soude]]

# Instructions
---
1. Dans une casserole, faites chauffer l'eau, le miel, le sucre, le beurre, les zestes d'orange hachés et la badiane
2. Mélangez. Retirez du feu aux premiers frémissements
3. Laissez infuser 15m puis retirez la badiane
4. Dans un cul-de-poule, versez la faine, les épices, la levure chimique et le bicarbonate
5. Versez le contenu de la casserole dans le cul-de-poule puis fouettez jusqu'à l'obtention d'une pâte homogène
6. Laissez reposer la pâte une bonne heure au réfrigérateur
7. Préchauffez votre four à 200°C (th. 6/7) puis répartissez la pâte dans les moules individuels.
8. Recouvrez chaque nonnette d'une cuillère à café de marmelade
9. Faites cuire au four à 200°C pendant 20m
10. Pour le glaçage, Mélangez le sucre glace avec le Rhum
11. Dès la sortie du four, glacez-en les nonnettes avec un pinceau puis démoulez-les après les avoir fait refroidir

#Dessert #Français #Sucré
