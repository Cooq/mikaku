---
date: 2024-06-23T16:04
image:
  - /images/Feuilletine.webp
---
# Introduction
---

Le croquant du royal

| Sujet                | Donnée    |
| -------------------- | --------- |
| Par                  | Adam      |
| Pour                 | 2 royales |
| Temps de préparation | 5min      |
| Durée de cuisson     |           |
| Temps total          | 5min      |

# Ingrédients
---
- 149g de [[praliné]]
- 37g de [[chocolat au lait]]
- 75g de [[crêpe dentelle]] en brisure (ou de la feuilletine)
- 15g de [[beurre]] (facultatif)
# Instructions
---
1. Faire fondre le chocolat avec le beurre
2. Mélanger avec le praliné réchauffé avec la crêpe dentelle

#Base #Français #Sucré #Dessert 