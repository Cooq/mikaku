---
date: 2024-06-23T16:13
image:
---
# Introduction
---

Une mousse au chocolat pas trop sucrée pour un royal réussi

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 6 personnes |
| Temps de préparation | 10min       |
| Durée de cuisson     |             |
| Temps total          | 10min       |

# Ingrédients
---
 - 230g de [[crème fraiche]] 35%MG
- 183g de [[chocolat noir]]
## Crème anglaise
 ![[Crème anglaise#Ingrédients]]

# Instructions
---
## Crème anglaise
![[Crème anglaise#Instructions]]
- Mettre la crème anglaise directement dans le chocolat et mélanger, si tout le chocolat ne fond pas, réchauffer au bain maris
- Faire monter la crème au batteur, d'abord lentement pour faire rentrer de l'air puis de plus en plus vite jusqu'à ce qu'elle soit montée
- Mélanger la crème montée avec le mélange chocolaté avec un fouet sans battre, juste en faisant des quarts de tour

#Sucré  #Dessert 