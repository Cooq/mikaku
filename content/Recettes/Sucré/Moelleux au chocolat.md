---
date: 2024-02-05T14:34
image:
  - /images/moelleux.webp
---
# Introduction
---

Fondant et chocolaté, tout ce qu'on en attend

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 9 personnes |
| Temps de préparation | 20m         |
| Durée de cuisson     | 25m         |
| Temps total          | 45m         |


# Ingrédients
---
- 125g [[farine]]
- 250g [[sucre]]
- 125g [[beurre]]
- 200g [[chocolat noir]]
- 4 [[œufs]]
- 0.5 sachet de [[levure chimique]]

# Instructions
---
1. Préchauffez le four à 180°C (Th 6)
2. Faites fondre le chocolat au bain-marie
3. Ajoutez le beurre
4. Dans un saladier, mélangez les œufs et le sucre, la levure puis la farine
5. Versez le chocolat fondu, puis mélangez jusqu'à obtention d'une pâte homogène
6. Versez la préparation dans un moule à manqué beurré et fariné
7. Faites cuire environ 25 min (adaptez le temps de cuisson pour obtenir un cœur plus ou moins fondant)

#Dessert #Français #Sucré
