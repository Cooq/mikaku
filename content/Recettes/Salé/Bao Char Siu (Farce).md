---
date: 2024-02-05T14:16
image:
  - /images/bao.webp
---
# Introduction
---

Farce au porc pour les baos (sauce bbq chinoise)

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | Adam   |
| Pour                 | 10 bao |
| Temps de préparation | 1h     |
| Durée de cuisson     | 15m    |
| Temps total          | 1h15   |


# Ingrédients
---
- 500 g [[échine de porc]]
- 15 g [[cassonade]]
- 2 cas de [[sauce soja]]
- 1 très grosse cas de [[sauce d'huître]]
- 2 cas [[sauce hoisin]]
- 2 cl [[alcool de riz]]
- 6 cl eau
- 15 g [[maizena]]
- [[poivre]]
- [[Bao (pain)]]

# Instructions
---
1. Coupez l'échine de porc en dés de 1cm
2. Réunissez dans une casserole la cassonade, les sauces, l'alcool de riz et l'eau
3. Portez à ébullition
4. Ajoutez la viande, poivrez
5. Faites cuire 10min en remuant de temps en temps
6. Délayez la maïzena avec un peu d'eau
7. Versez dans la casserole, remuez constamment et laissez bouillonner jusqu'à épaississement
8. Réserver au frais jusqu'au lendemain
9. Suivre la recette [[Bao (pain)]]

#Plat #Asiatique #Salé
