---
date: 2024-06-18T16:11
image:
---
# Introduction
---

Ça tient au corps, c'est bon en plus

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 4 personnes |
| Temps de préparation | 10min       |
| Durée de cuisson     | 15min       |
| Temps total          | 25min       |

# Ingrédients
---
- 1 [[oignon]]
- 1 gousse d'[[ail]]
- 4cl d'[[huile d'olive]]
- 2 gousses de [[cardamone]]
- 2 [[piment rouge]] séchés
- 2 [[clous de girofles]]
- 1 cuillère à café de [[curry]] en poudre
- 1 cuillère à café de [[cannelle]] en poudre
- 70g de [[concentré de tomates]]
- 250g de [[lentilles corail]]
- 10cl de [[lait de coco]] (ou 7cl d'eau)
- le [[jus de citron|jus d'un demi citron]]
- [[sel]]
# Instructions
---
1. Éplucher l'oignon et l'ail
2. Émincer l'oignon et le faire revenir 5min dans l'huile d'olive jusqu'à ce qu'il soit caramélisé
3. Ajouter l'ail pressé au presse-ail, les gousses de cardamine écrasées, les piments et toutes les épices et faire revenir le tout en 2min
4. Verser 50cl d'eau puis incorporer le concentré de tomate, les lentilles corail et laisser mijoter 10min à couvert
5. Saler et ajouter le lait de coco en fin de cuisson
6. Après cuisson, vous pouvez ajuster la quantité d'eau en fonction de la texture souhaitée

#Plat #Indien #Salé #IGBas #Végétarien