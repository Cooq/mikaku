---
date: 2024-04-17T08:54
image:
---
# Introduction
---

Sans jambon ça marche aussi

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | Adam   |
| Pour                 | 1 cake |
| Temps de préparation | 10m    |
| Durée de cuisson     | 45m    |
| Temps total          | 55m    |

# Ingrédients
---
- 75g d'[[olives]]
- 100g d'[[emmental]] râpé
- 12.5cl de [[lait]]
- 3 [[œufs]]
- 200g de [[jambon|dés de jambon]]
- 150g de [[farine]]
- 1 pincée de [[sel]]
- 10cl d'[[huile de tournesol]]
- 1 pincée de [[poivre]]
- 1 sachet de [[levure chimique]]

# Instructions
---
1. Préchauffez le four à 180°C
2. Découpez le jambon et les olives en morceaux
3. Dans un saladier, mélangez au fouet les œufs, la farine, la levure et le sel/poivre
4. Incorporez petit à petit l'huile et le lait chaud
5. Ajoutez l'emmental râpé
6. Incorporez le jambon et les olives à la pâte
7. Ajoutez le tout dans un moule non graissé et faire cuire au four pendant 45min

#Plat #Français #Salé
