---
date: 2023-03-13T09:50:00.000Z
approved: true
image:
  - /images/poulpettes.webp
---
# Poulpettes
---
Efficace et pas cher, c'est les poulpettes que j'préfère

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Teresa      |
| Pour                 | 3 personnes |
| Temps de préparation | 10m         |
| Durée de cuisson     | 30m         |
| Temps total          | 40m         |


# Ingrédients
---
- 100g de [[parmesan]]
- 85g de [[chapelure]] (ou pain rassi)
- 1L de [[sauce tomate]]
- 5 à 10g de [[persil]]
- 4 [[œufs]]
- 2 gousses d'[[ail]]
- [[huile d'olive]]

# Instructions
---
1. Hacher finement l'ail
2. Mélanger le parmesan, les œufs, l'ail et le persil
3. Ajouter la chapelure jusqu'à obtenir une pâte sèche et consistante
4. Faire chauffer de l'huile
5. Faire des petits pâtés plats et les faire roussir dans l'huile
6. Une fois cuits, mettre les pâtés dans un sopalin pour les enlever le surplus d'huile
7. Faire cuire les pâtés dans la moitié de la sauce tomate jusqu'à ce que ce soit bien imbibé
8. Mettre le reste de sauce tomate puis servir chaud

#Plat #Italien #Végétarien #Salé