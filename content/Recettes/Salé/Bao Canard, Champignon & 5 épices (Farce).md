---
date: 2024-02-05T14:11
image:
  - /images/bao.webp
---
# Introduction
---

Farce pour les Bao

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | Adam   |
| Pour                 | 8 bao  |
| Temps de préparation | m      |
| Durée de cuisson     | m      |
| Temps total          | m      |


# Ingrédients
---
- 2 cuisses de [[canard confit]] en conserve
- 1 gros [[oignon]]
- 3 grosses gousses d'[[ail]]
- 500 g [[champignons de Paris]]
- 2 grosses cas de [[sauce hoisin]]
- 1 cac de [[cinq-épices chinois]]
- 1 cac [[poivre]] de sichuan
- 1 petite botte de [[persil]] plat
- 1 botte de [[ciboulette]]
- 1 petite botte d'[[aneth]]
- [[sel]] et [[poivre]]
- [[Bao (pain)]]

# Instructions
---
1. Nettoyez les champignons, coupez les plus gros en lamelles
2. Émincez l'oignon, pressez les gousses d'ail
3. Ciselez les herbes, réservez 3 cuillerées à soupe pour la pâte
4. Retirez la peau des cuisses de canard et défaites grossièrement la chair avec une fourchette
5. Les gros morceaux de chair s'effilocheront pendant la cuisson
6. Réservez 3 cuillerées à soupe de graisse de canard
7. Concassez le poivre de Sichuan
8. Chauffez la graisse de canard dans une poêle
9. Faites suer l'oignon 2min puis ajoutez les champignons
10. Salez, poivrez
11. Faites revenir jusqu'à ce que les champignons commencent à dorer
12. Ajoutez la chair de canard, l'ail, la sauce hoisin, le cinq-épices et le poivre de Sichuan
13. Poursuivez la cuisson 5min en remuant de temps en temps
14. Goûtez, rectifiez l'assaisonnement si nécessaire
15. Hors du feu, ajoutez les herbes
16. Laissez refroidir complètement
17. Laissez reposer 1 nuit
18. Suivre la recette [[Bao (pain)]]

#Plat #Asiatique #Salé