---
date: 2023-03-13T09:50:00.000Z
image:
  - /images/spaghetti-sardine.webp
---
# Introduction
---

En fait, c'est pas des Spaghetti mais des Capellini. Mensonges !

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Teresa      |
| Pour                 | 3 personnes |
| Temps de préparation | 2m          |
| Durée de cuisson     | 3m          |
| Temps total          | 5m          |


# Ingrédients
---

- 2 grosses gousses d'[[ail]]
- 95g de [[sardine]] à l'[[huile d'olive]]
- 250g de [[capellini]] n°1

# Instructions
---

1. Hâcher finement l'ail
2. Faire chauffer l'eau et l'huile des sardines dans une poele
3. En même temps, mettre l'ail dans la poele et les pates dans l'eau
4. Attendre que l'ail devienne rosé puis écraser les sardines dedans
5. Mélanger les deux lorsque les pates sont cuites (~3min)


#Plat #Italien #Salé