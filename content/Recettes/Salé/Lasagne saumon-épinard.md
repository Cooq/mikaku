---
date: 2024-02-05T14:26
image:
  - /images/lasagne-epinard-saumon.webp
---

# Introduction
---

L'alliance de la Terre et la Mer en un seul repas

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 4 personnes |
| Temps de préparation | 15m         |
| Durée de cuisson     | 25m         |
| Temps total          | 40m         |


# Ingrédients
---
- 1 pincée sel
- 5 g [[beurre]]
- 1 boîte [[lasagnes]] précuites
- 300 g [[saumon]] fumé
- 500 g [[épinards]] en branches surgelés
- 50 g [[parmesan]]
- 50 cl [[crème fraiche]] liquide
- 1 [[bouillon de volaille]]
- 1 pincée poivre

# Instructions
---
1. Préchauffer le four à 210°C
2. Faire cuire les épinards avec un bouillon de volaille
3. Mettre un peu de beurre dans le plat, puis alterner une couche de lasagnes, une couche de saumon, une couche d'épinards et un peu de parmesan jusqu'à la l'épuisement des ingrédients
4. Saler, poivrer la crème liquide. La verser dans le plat
5. Saupoudrer de parmesan puis cuire à four chaud pendant 25 min

#Plat #Italien #Végétarien #Salé