---
date: 2024-02-05T14:54
image:
  - /images/risotto.webp
---
# Introduction
---

Des champignons, du riz, de quoi bien requinquer !

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Serge       |
| Pour                 | 2 personnes |
| Temps de préparation | 5m          |
| Durée de cuisson     | 20m         |
| Temps total          | 25m         |


# Ingrédients
---
- 20ml eau
- 1 cuillère à soupe huile
- 500g [[champignons de Paris]]
- 50g [[riz]]
- 1 [[bouillon de volaille]]

# Instructions
---
1. Faire revenir le riz dans l'huile jusqu'à ce que le riz devienne blanc
2. Ajouter les champignon avec l'eau et le bouillon de poule émietté
3. Laisser cuire à feu doux jusqu'à ce que l'eau soit évaporée

#Plat #Français #Végétarien #Salé