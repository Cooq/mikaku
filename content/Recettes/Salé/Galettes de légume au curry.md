---
date: 2024-06-18T19:49:00
---

# Introduction
---

Des petites galettes bien bonnes

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 2 personnes |
| Temps de préparation | 15m         |
| Durée de cuisson     | 15m         |
| Temps total          | 30m         |

# Ingrédients
---
- 200g de [[patate douce]] (ou [[carotte]])
- 200g de [[courgette]]
- 1 [[échalote]]
- 40g de [[son d'avoine]]
- 2 [[œufs]]
- 2 cuillères à café de [[curry]]
- 1/2 cuillère à café de sel
- [[huile d'olive]]

# Instructions
---
1. Peler et râper les légumes dans un saladier. Saler et mélanger
2. Peler et ciseler l'échalote
3. Presser les légumes avec les mains pour éliminer un maximum d'eau. Mélanger les légumes avec l'échalote, le son d'avoine, les œufs et le curry
4. Faire chauffer une poêle avec de l'huile d'olive et réaliser de petites galettes à l'aide d'une cuillère. Laisser cuire 3min de chaque côté à feu vif
5. Déguster les galettes chaudes accompagnées d'une salade


#Plat #IGBas  #Végétarien #Salé