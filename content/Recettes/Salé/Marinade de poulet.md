---
date: 2024-02-05T14:29
image:
  - /images/marinade.webp
---
# Introduction
---

Parce qu'il faut des fois changer du poulet au beurre

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Teresa      |
| Pour                 | 2 personnes |
| Temps de préparation | 2h          |
| Durée de cuisson     | 10m         |
| Temps total          | 2h10        |


# Ingrédients
---
- 2 [blancs de poulets](blancs%20de%20poulet.md)
- 2 gousses d'[[ail]]
- 2 cuillères à soupe [[huile d'olive]]
- 2 cuillères à soupe [[jus de citron]]
- 1 pincée [[thym]]
- 1 pincée [[herbes de provence]]

# Instructions
---
1. Émincer l'ail
2. Découper le poulet en dés
3. Arroser le poulet d'huile d'olive, de citron et malaxer avec l'ail
4. Saler et poivrer. En fonction des goûts, rajouter du thym ou des herbes de Provence
5. Laisser macérer 2h
6. Faites des brochettes avant de les cuire à la poêle

#Plat #Français #Salé