---
date: 2024-02-05T14:53
image:
  - /images/pizza.webp
---
# Introduction
---

Moelleuse et croustillante, elle saura vous réconforter dans les soirées les plus dures

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Teresa      |
| Pour                 | 4 personnes |
| Temps de préparation | 1h          |
| Durée de cuisson     | 3m          |
| Temps total          | 3h          |


# Ingrédients
---
- 250ml eau
- 500g [[farine]]
- 4 cuillères à soupe [[huile d'olive]]
- 1 cuillère à café sel
- 1 sachet [[levure de boulanger]]
- 1 pincée [[sucre]]

# Instructions
---
1. Mélanger le sel avec la farine
2. Dans un bol à part, réhydrater la levure avec un peu d'eau tiède et une pincée de sucre
3. Une fois que le volume a doublé, malaxer le mélange avec la farine en y incorporant le reste d'eau et enfin l'huile d'olive
4. Laisser reposer 2h

#Plat #Italien #Salé