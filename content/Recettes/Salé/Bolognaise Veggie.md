---
date: 2021-02-24T23:00
---
# Introduction
---

Aussi gourmand que les pâtes bolognaise classiques, voire plus !

| Sujet                | Donnée     |
| -------------------- | ---------- |
| Par                  | Jow        |
| Pour                 | 1 personne |
| Temps de préparation | 03m        |
| Durée de cuisson     | 21m        |
| Temps total          | 24m        |

# Ingrédients
---

- 100 g [[pâtes]] (spaghetti)
- 50 g [[lentilles]] (cuites)
- 1/2 [[carotte]] (frais)
- 150 g [[purée de tomates]]
- 1/2 gousse d'[[ail]]
- 1/4  d'[[oignon]] jaune

# Instructions
---

1. Épluchez puis râpez les carottes à l'aide d'une râpe. (Vous pouvez aussi la couper très finement).
2. Épluchez puis émincez finement les oignons.
3. Dans une poêle ou une casserole, faites chauffer un filet d'huile d’olive. Ajoutez les oignons dans la poêle, râpez l’ail et ajoutez les carottes.
4. Laissez cuire à feu moyen pendant 2 minutes en remuant régulièrement. Salez et poivrez.
5. Ajoutez la sauce tomate (si vous voyez que la préparation n'est pas assez liquide, n'hésitez pas à ajouter un peu d'eau). Salez et poivrez. Couvrez et laissez mijoter 15 minutes sur feu doux.
6. Pendant ce temps, portez une casserole d’eau à ébullition. Salez et ajoutez les spaghettis. Faites cuire selon les instructions du paquet (soit environ 8 minutes). Égouttez les spaghettis et réservez.
7. Au bout de 15 minutes de cuisson, ajoutez les lentilles et mélangez. Poursuivez la cuisson encore 4 minutes tout en remuant.
8. Ajoutez les spaghettis dans la sauce et mélangez.
9. Servez dans des assiettes, ajoutez un filet d'huile d'olive, poivrez, c'est prêt ! Optionnel : vous pouvez ajouter quelques feuilles de basilic.

#Plat #Italien #Salé #Végétarien