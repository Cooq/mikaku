---
date: 2024-06-18T16:24
image:
---
# Introduction
---

Un savant mélange de quinoa et d'autre trucs

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 2 personnes |
| Temps de préparation | 10min       |
| Durée de cuisson     | 10min       |
| Temps total          | 20min       |

# Ingrédients
---
- 1 [[courgette]]
- 1 pincée de [[sel]]
- ½ [[oignon]]
- 250g de [[quinoa]] cuit (ou 120g de [[quinoa]] cru)
- 2 [[œufs]]
- 2 cuillères à soupe de [[chapelure]]
- 50g de [[parmesan]] râpé
- [[huile végétale]] pour la cuisson
# Instructions
---
1. Peler et râper la courgette dans un récipient
2. Ajouter une pincée de sel, mélanger et laisser dégorger 2min
3. Pendant ce temps, peler et ciseler le demi-oignon
4. Presser la courgette avec les mains pour éliminer un maximum d'eau
5. Mélanger tous les ingrédients dans un saladier
6. Faire chauffer une poêle huilée. Former des galettes avec une cuillère à soupe et les faire dorer 2min de chaque côté
7. Servir les galettes bien chaudes, accompagnées d'une salade

#Plat #Salé #IGBas #Végétarien