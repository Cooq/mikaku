---
date: 2024-06-23T18:22
image:
---
# Introduction
---

Long à faire, je sais pas si ça vaut le coût

| Sujet                | Donnée            |
| -------------------- | ----------------- |
| Par                  | Adam              |
| Pour                 | 3 tartes au moins |
| Temps de préparation | 8h                |
| Durée de cuisson     |                   |
| Temps total          | 8h                |

# Ingrédients
---
## Beurre Manié
- 320g de [[beurre]]
- 130g de [[farine]] T65

## Détrempe
- 290g de [[farine]] T65
- 100g de [[beurre]]
- 8g de [[sel]]
- 50g d'eau
- 3g de [[vinaigre blanc]] (pour blanchir la farine)
# Instructions
---
## Beurre Manié
1. Battre le beurre avec un batteur (type kitchenaid) muni d'une feuille pour qu'il soit un peu plus malléable 
2. Mélanger le beurre et la farine
3. Mettre au frigo
## Détrempe
1. Toujours avec le batteur muni de la feuille
2. Mélanger la farine, le sel
4. Ajouter l'eau et le vinaigre. Suivant la farine, la quantité d'eau peu varier. *Ajouter le minimum d'eau possible, dès qu'une boule est formée, n'en rajoutez pas plus*
5. Mettre au frigo 20min
## Feuilletage
1. À l'aide d'un rouleau à pâtisserie, aplatir le beurre manié et la détrempe, le beurre doit pouvoir envelopper la détrempe
2. Envelopper la détrempe du beurre et aplatir
3. Replier la pâte sur elle même en 3 plis (comme un papier dans une enveloppe) et aplatir
4. Mettre au frigo 2h
5. Replier la pâte sur elle-même en 3 plis 2 fois d'affilé puis mettre au frigo, puis mettre en frigo 2h
7. Répéter encore 2 fois cette opération (vous devriez avoir replié la pâte entre 5 et 7 fois) 

#Base #Sucré #Salé #Français 