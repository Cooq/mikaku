---
date: 2024-02-05T12:03
image:
---

# Introduction
---
La sagne ou le sagne ? Telle est la question

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 4 personnes |
| Temps de préparation | 15m         |
| Durée de cuisson     | 40m         |
| Temps total          | 55m         |

# Ingrédients
---
- 8-10 plaques de [[lasagnes]]
- 2 [[courgette]]
- 1 [[aubergine]]
- 1 [[poivron]] rouge
- 1 [[poivron]] jaune
- 1 boite de [[tomate]] pelées ou concassées
- 1 [[oignon]]
- 1-2 gousses d'[[ail]]
- [[huile d'olive]]
- [[thym]] ou [[basilic]] ou [[origan]] ou [[herbes de provence]]
- 200g de [[mozzarella]]
- 100g d'[[emmental]] râpé

# Instructions
---
1. Couper tous les légumes en morceaux
2. Couper la mozzarella en tranches
3. Hacher l’ail et l’oignon
4. Préchauffer le four à 180°C
5. Dans une casserole, faire revenir l’ail et l’oignon dans un peu d’huile d’olive. Quand les oignons deviennent translucide, ajouter tous les légumes
6. Faire revenir 10min
7. Ajouter les tomates pelées, saler, poivrer, ajouter les herbes de votre choix
8. Laisser cuire 15-20min
9. Dans un plat, alterner les légumes, la mozzarella et les pâtes en finissant par les légumes
10. Saupoudrer de fromage râpé
11. Mettre au four 25min
12. Les lasagnes sont cuites lorsque le fromage est gratiné

Notes:
Il est possible de remplacer les lasagnes par des tranches d’aubergines grillées en faisant une recette #IGBas

#Plat #Italien #Végétarien #Salé