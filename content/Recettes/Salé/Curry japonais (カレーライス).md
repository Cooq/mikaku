---
date: 2024-02-05T14:05
image:
  - /images/curry-japonais.webp
---
# Introduction
---

Le curry japonais est l'un des plats les plus populaires au Japon. Il est habituellement plus épais et a un goût plus doux et moins épicé que son équivalent indien.

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Samuel      |
| Pour                 | 4 personnes |
| Temps de préparation | 10m         |
| Durée de cuisson     | 1h          |
| Temps total          | 1h10        |

# Ingrédients
---
- 300 g [[bœuf]], [[blancs de poulet]], [[agneau]] ou [[crevette]]
- 300 g [[oignon]]
- 80 g [[carotte]]
- 100 g [[pomme de terre]]
- 2 c à soupe d'[[huile végétale]]
- 450 ml eau
- [[pâte de curry]] (1/4 d'un bloc Golden Curry)

# Instructions
---
1. Émincés tous les ingrédients
2. Dans une grande poêle, faire sauter la viande et les légumes dans l'huile, à feu moyen pendant environ 5 min.
3. Ajouter de l'eau et porter à ébullition. Réduire le feu, couvrir et laisser mijoter jusqu'à ce que les ingrédients soient tendres, pendant environ 15 min.
4. Retirer du feu, casser en morceaux la pâte de curry et les verser dans la poêle. Remuer jusqu'à ce que les morceaux soient bien incorporés. Laisser mijoter environ 5 min. tout en remuant.
5. Servir chaud avec du riz

#Plat #Asiatique #Salé
