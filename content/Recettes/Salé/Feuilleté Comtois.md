---
date: 2024-02-05T14:19
image:
---
# Introduction
---

Aussi appelé Croûte au Morilles

| Sujet                | Donnée       |
| -------------------- | ------------ |
| Par                  | Teresa       |
| Pour                 | 2 feuilletés |
| Temps de préparation | 55m          |
| Durée de cuisson     | 20m          |
| Temps total          | 1h15         |


# Ingrédients
---
- 1 noix [[beurre]]
- 2 [[carotte]]
- 1 [[échalote]]
- 20 cl [[crème fraiche]]
- 200 g [[comté]]
- 2 cuillères à café [[moutarde]]
- 1 [blanc de poulet](blancs%20de%20poulet.md)
- 1 [[Ingrédients/pâte feuilleté]]
- 10 g [[noix]]

# Instructions
---
1. Faire revenir une échalotes dans du beurre avec les carottes préalablement lavées épluchées et coupées en petits morceaux. Bien faire dorer puis mettre à petit feu sans faire brûler
2. Y mettre 2 verre d'eau. Saler, poivrer. Couvrir, compter 15 minutes après ébullition.
3. Ajouter le blanc de poulet et laisser cuire 20 minutes toujours à petit bouillon.
4. Pendant ce temps, râper le comté et faire 4 cercles avec la pâte feuilletée.
5. Une fois le poulet prêt le couper en petit dés.
6. Continuer de faire bouillir les carottes jusqu'à ce que le jus diminue de moitié.
7. Ajouter la crème entière liquide laisser encore quelques minutes, le temps de faire réchauffer la crème et passer au mixeur.
8. Sur la moitié des cercles poser une cuillère à café de moutarde, du comté, le poulet et finir avec le reste de comté et poser une morille et des noix.
9. Fermer avec le deuxième cercle.
10. Passer au four 20 minutes à 210°C

#Plat #Français #Salé