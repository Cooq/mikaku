---
date: 2024-06-21T07:50
image:
---
# Introduction
---

Score santé 📉
Score dégustation 📈

| Sujet                | Donnée  |
| -------------------- | ------- |
| Par                  | Adam    |
| Pour                 | 1 tarte |
| Temps de préparation | 5min    |
| Durée de cuisson     | 25min   |
| Temps total          | 30min   |

# Ingrédients
---
- 500g de [[tomate|tomates]] cerises
- 1 [[Ingrédients/pâte feuilleté]]
- 40g de [[beurre demi-sel]]
- 40g de [[sucre]] en poudre
- 4 cuillères à soupe de [[sauce soja]]

# Instructions
---
1. Préchauffez le four à 200°C
2. Dans une poêle feu vif, versez la **sauce soja**, le **sucre** et le **beurre**. Faites réduire pour faire un caramel
3. Coupez en deux les **tomates cerises** lavées
4. Récupérez le papier de votre pate feuilletée et ajoutez le dans le fond de votre moule pour faciliter le démoulage
5. Versez le caramel dans le fond de votre moule et disposez les tomates avec la partie bombée vers le bas
6. Disposez la pâte feuilletée sur le dessus en repliant les bords vers l'intérieur
7. Faites une cheminée à l'aide d'un petit couteau puis enfournez le tout à 200°C pendant 20min
8. Une fois bien dorée, sortez la tarte du four. Laissez-la refroidir complètement avant de la démouler. Pour ce faire : Placez une grande assiette sur le dessus du moule puis retournez d'un coup sec ! *Attention ! Il peut y avoir un peu de jus*
9. Servez la tatin avec quelques feuilles de basilic sur le dessus et une salade verte pour accompagner

#Plat  #Salé #Végétarien #Français #Sucré  