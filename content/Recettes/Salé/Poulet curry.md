---
date: 2024-01-27T12:00:00.000Z
image:
  - /images/curry-poulet.webp
---
# Introduction
---

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Nicolas     |
| Pour                 | 4 personnes |
| Temps de préparation | 10m         |
| Durée de cuisson     | 25m         |
| Temps total          | 35m         |

# Ingrédients
---
- 4 [[blancs de poulet]]
- 600g de [[potimarron]] (1 potimarron)
- 2 briques de 200ml de [[lait de coco]] (ou de [[crème fraiche]])
- 3 cuillères à soupe de [[curry]] en poudre
- 5 cuillères à soupe de sauce [[nuoc mâm]]

# Instructions
---
1. Couper le poulet en morceaux
2. Eplucher et couper le potimarron
3. Faire saisir 5 minutes les blancs de poulets avec 2 cuillères à soupe d'huile
4. Ajouter les dès de potimarrons et faire cuire 15 min en remuant (le mieux est de la faire dans un fait-tout avec un couvercle)
5. Ajouter les deux briques de lait, la sauce Nuoc Mâm et le curry. Faire cuire 10 minutes

#Plat #Asiatique #Salé