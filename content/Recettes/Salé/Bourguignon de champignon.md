---
date: 2021-12-22T13:24
---
# Introduction
---

Encore meilleur parsemé d'herbes fraîches.

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | ELLE   |
| Pour                 | 4      |
| Temps de préparation | 10m    |
| Durée de cuisson     | 40m    |
| Temps total          | 50m    |


# Ingrédients
---

- 1 kg de [[champignons de Paris]]
- 1 [[carotte]] moyenne
- 120 g de petits [[oignons nouveaux]]
- 2 [[oignon]] moyens
- 2 gousses d'[[ail]]
- 2 cuillère(s) à soupe d'[[huile d'olive]]
- 1 cuillère(s) à café de [[thym]] frais effeuillé
- 2 cuillère(s) à soupe de [[farine]]
- 25 cl de [[vin rouge]]
- 2 cuillère(s) à soupe de [[concentré de tomates]]
- 50 cl de [[bouillon de légumes]]
- [[sel]], [[poivre]] du moulin

# Instructions
---

1. Rincez les champignons et coupez-les en deux
2. Epluchez la carotte et coupez-la en rondelles
3. Hachez les oignons nouveaux
4. Epluchez et hachez finement les oignons et l'ail.
5. Faites chauffer l'huile dans une casserole à fond épais. Laissez-y blondir les champignons et les oignons nouveaux pendant 3 à 4 min. Réservez hors du feu.
6. Faites revenir dans la casserole la carotte, les oignons et le thym pendant 7 à 8 min sur feu moyen, jusqu'à ce que le tout soit caramélisé. Assaisonnez légèrement. Ajoutez l'ail et poursuivez la cuisson pendant 1 à 2 min. Parsemez de farine, remuez et poursuivez la cuisson encore 1 à 2 min.
7. Versez le vin. Remuez en grattant le fond de la casserole, puis augmentez le feu et laissez une partie du vin s'évaporer. Ajoutez le concentré de tomates, le bouillon de légumes, les champignons, les oignons nouveaux et portez à ébullition.
8. Laissez mijoter sur feu doux pendant 15 à 20 min, jusqu'à ce que la sauce soit bien épaisse. Vérifiez l'assaisonnement et servez aussitôt accompagné d'une purée de pommes de terre ou de céleri-rave.

#Plat #Français #Salé