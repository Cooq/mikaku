---
date: 2024-02-02T15:00
image:
---

# Chili con carottes
---

Le chili, mais avec des carottes

| Sujet                | Donnée      |
| -------------------- | ----------- |
| Par                  | Adam        |
| Pour                 | 4 personnes |
| Temps de préparation | 20m         |
| Durée de cuisson     | 30m         |
| Temps total          | 1h10        |


# Ingrédients
---
- 400g de [[haricots rouges]]
- 380g de [[tomate]] pelée
- 200g de [[oignon]]
- 100g de [[carotte]]
- 80g de [[poivron]] rouge
- 12g de [[cassonade]]
- 12,5 cl d'eau
- 3 cuillère à soupe de [[huile d'olive]]
- 2 gousses d'[[ail]]
- 1 cuillère à café de [[piment rouge]] séché
- 1 cuillère à café de [[cumin]] en poudre
- Sel poivre

# Instructions
---
1. Pelez et hachez l'oignon et l’ail. Hachez finement le piment rouge. Épluchez et coupez en petits dés les carottes et le poivron.
2. Chauffez l'huile d'olive à feu moyen dans un poêlon. Faites-y revenir l'oignon et l'ail environ 2 minutes, jusqu'à ce qu'ils deviennent translucides, en remuant de temps à autre. Ajoutez les carottes et le poivron. Laissez cuire 4 à 5 min, en remuant régulièrement.
3. Ajoutez le piment rouge, le cumin et la cassonade. Laissez cuire le tout 1 min en mélangeant. Comptez 8 à 10 min.
4. Égouttez les haricots rouges, rincez-les sous l'eau claire et égouttez de nouveau. Ajoutez-les à la préparation sur le feu.
5. Ajoutez les tomates coupées en dés et assez d'eau pour que le mélange reste assez humide.
6. Portez à ébullition, mélangez, couvrez et laissez cuire à feu doux environ 10 min. Salez et poivrez.


#Plat #AmériqueDuSud #Végétarien #Salé