---
date: 2024-02-05T13:50
image:
  - /images/bao.webp
---
# Introduction
---

Mode opératoire pour faire les baos. À vous de choisir une farce adéquate

| Sujet                | Donnée |
| -------------------- | ------ |
| Par                  | Adam   |
| Pour                 | 10 bao |
| Temps de préparation | 1h     |
| Durée de cuisson     | 15m    |
| Temps total          | 1h15   |


# Ingrédients
---
- 300 g [[farine]] T55 tamisée
- 8 g [[levure de boulanger]]
- 40 g [[sucre]]
- 1 cac [[bicarbonate de soude]]
- 1 cac bombée de [[sel]]
- 165 g eau tiède
- 1.5 cas d'[[huile végétale]]
- farce à bao ([[Bao Canard, Champignon & 5 épices (Farce)]], [[Bao Char Siu (Farce)]])

# Instructions
---
1. Préchauffer le four à 30°C
2. Dans un récipient, mélangez tous les ingrédients de la pâte
3. Ajoutez les ingrédients précisés dans la recette de la farce
4. Faites un puits, ajoutez l'huile au centre puis versez l'eau tiède
5. Mélangez à la main
6. Lorsque la pâte commence à s'amalgamer, versez-la sur le plan de travail et pétrissez jusqu'à obtenir une belle pâte lisse et homogène (~5min)
7. La pâte ne doit pas coller au mains
8. Formez une boule, couvrez d'un torchon propre et laissez lever 1h dans un endroit chaud et à l'abri des courants d'air (le four éteint par exemple)
9. La pâte doit doubler de volume
10. Découpez 10 carrés de papier sulfurisés (6-7cm de côté)
11. Divisez la pâte en 10 boules
12. Farinez le plan de travail et étalez chaque boule de pâte en prenant soin d'avoir le centre de la pâte plus épais que les bords
13. Déposez une boule de farce et procédez au pliage
14. Posez chaque bao sur un carré de papier sulfurisé puis dans le panier vapeur. Les bao doivent être bien espacés
15. Laissez lever 1h sous un torchon
16. Cuisez 15min à la vapeur
17. Dégustez les bao à peine tiédis

#Plat #Asiatique #Salé