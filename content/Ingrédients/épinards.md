# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 28 kcal    |
| Matières grasses                                        | 0,538 g    |
| Acides gras saturés                                     | 0,078 g    |
| Glucides                                                | 1,45 g     |
| Sucres                                                  | 0,434 g    |
| Fibres alimentaires                                     | 2,57 g     |
| Protéines                                               | 3,04 g     |
| Sel                                                     | 0,243 g    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 100 %      |