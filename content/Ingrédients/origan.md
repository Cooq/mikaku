# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 9 kcal     |
| Matières grasses     | ?          |
| Acides gras saturés  | 0,296 g    |
| Glucides             | ?          |
| Sucres               | 0,454 g    |
| Fibres alimentaires  | ?          |
| Protéines            | 1,33 g     |
| Sel                  | ?          |