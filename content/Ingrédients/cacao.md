# Tableau Nutritionnel
Le tableau nutritionnel est celui du VanHouten

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 379 kcal   |
| Matières grasses     | 21 g       |
| Acides gras saturés  | 13 g       |
| Glucides             | 11 g       |
| Sucres               | 0,9 g      |
| Fibres alimentaires  | 28 g       |
| Protéines            | 20 g       |
| Sel                  | 0,08 g     |
| Cacao (minimum)      | 100 %      |
