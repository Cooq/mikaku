# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              |  29 kca    |
| Matières grasses     |  0,442     |
| Acides gras saturés  |  0,1       |
| Glucides             |  2,1       |
| Sucres               |  1,38      |
| Fibres alimentaires  |  2,67      |
| Protéines            |  2,72      |
| Sel                  |  0,05      |
