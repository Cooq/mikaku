# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 46 kcal    |
| Matières grasses     | 1,95 g     |
| Acides gras saturés  | 0,248 g    |
| Glucides             | 6,47 g     |
| Sucres               | 1,16 g     |
| Fibres alimentaires  | 5,17 g     |
| Protéines            | 5,94 g     |
| Sel                  | 0,269 g    |