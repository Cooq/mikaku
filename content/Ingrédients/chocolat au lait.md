# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Pour 100 g |
| ----------------------------------------------------------------------------------------------------------- | ---------- |
| Énergie                                                                                                     | 552 kcal   |
| Matières grasses                                                                                            | 34,5 g     |
| Acides gras saturés                                                                                         | 19,6 g     |
| Acides gras monoinsaturés                                                                                   | 9,21 g     |
| Acides gras trans                                                                                           | 0,067 g    |
| Cholestérol                                                                                                 | 15 mg      |
| Glucides                                                                                                    | 51,9 g     |
| Sucres                                                                                                      | 48,9 g     |
| Sucres ajoutés                                                                                              | 36,8 g     |
| Polyols                                                                                                     | 38,9 g     |
| Fibres alimentaires                                                                                         | 2,53 g     |
| Protéines                                                                                                   | 7,17 g     |
| Sel                                                                                                         | 0,231 g    |
| Alcool                                                                                                      | 0 % vol    |
| Vitamine A (rétinol)                                                                                        | 27,3 µg    |
| Vitamine D                                                                                                  | 0,557 µg   |
| Vitamine C (acide ascorbique)                                                                               | 0,663 mg   |
| Potassium                                                                                                   | 1 380 mg   |
| Calcium                                                                                                     | 129 mg     |
| Fer                                                                                                         | 2,3 mg     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 7,23 %     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 16,1 %     |
| Cacao (minimum)                                                                                             | 33,9 %     |