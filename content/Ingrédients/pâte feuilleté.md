# Tableau Nutritionnel



| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | -------------------------------------- |
| Énergie                                                                                                      | 389 kcal               |
| Matières grasses                                                                                             | 20 g                                   |
| Acides gras saturés                                                                                          | 14 g                                   |
| Glucides                                                                                                     | 41 g                                   |
| Sucres                                                                                                       | 1,6 g                                  |
| Fibres alimentaires                                                                                          | 1 g                                    |
| Protéines                                                                                                    | 7,2 g                                  |
| Sel                                                                                                          | 1,1 g                                  |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0,55 %                                 |