

# Tableau Nutritionnel
| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 126 kcal   |
| Matières grasses     | 5,06 g     |
| Acides gras saturés  | 0,667 g    |
| Glucides             | 15,7 g     |
| Sucres               | 3,06 g     |
| Fibres alimentaires  | 3,13 g     |
| Protéines            | 4,72 g     |
| Sel                  | 0,999 g    |

