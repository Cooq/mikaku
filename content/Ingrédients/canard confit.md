# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 134 kcal   |
| Matières grasses     | 6 g        |
| Acides gras saturés  | 2,2 g      |
| Glucides             | 6,7 g      |
| Sucres               | 0,5 g      |
| Fibres alimentaires  | 0 g        |
| Protéines            | 11 g       |
| Sel                  | 0,8 g      |
