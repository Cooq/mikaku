# Tableau Nutritionnel

| Pour 100 g   | Teneur   |
| ------------ | -------- |
| Énergie      | 134 kcal |
| Eau          | 78,4 g   |
| Fibres       | 0 g      |
| Glucides     | 5 g      |
| Protéines    | 0,5 g    |
| Lipides      | 0 g      |
| Alcool       | 16.1 g   |

