# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 369 kcal |
| Matières grasses                                        | 6,11 g     |
| Acides gras saturés                                     | 0,755 g    |
| Glucides                                                | 61,7 g     |
| Sucres                                                  | 2,35 g     |
| Amidon                                                  | 65,3 g     |
| Fibres alimentaires                                     | 6,04 g     |
| Protéines                                               | 11,8 g     |
| Sel                                                     | 0,05 g     |
| Alcool                                                  | 0 % vol    |
| Vitamine A (rétinol)                                    | 44,4 µg    |
| Vitamine B1 (Thiamine)                                  | 0,577 mg   |
| Vitamine B3/PP (Niacine)                                | 4,22 mg    |
| Calcium                                                 | 130 mg     |
| Phosphore                                               | 292 mg     |
| Fer                                                     | 6,08 mg    |
| Magnésium                                               | 124 mg     |
| Zinc                                                    | 3,93 mg    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 5,26 %     |