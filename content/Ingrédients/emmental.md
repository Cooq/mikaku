# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 376 kcal |
| Matières grasses                                        | 29,3 g     |
| Acides gras saturés                                     | 19,7 g     |
| Glucides                                                | 0,661 g    |
| Sucres                                                  | 0,408 g    |
| Fibres alimentaires                                     | 0,122 g    |
| Protéines                                               | 27,6 g     |
| Sel                                                     | 0,655 g    |
| Alcool                                                  | 0 % vol    |
| Calcium                                                 | 970 mg     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |