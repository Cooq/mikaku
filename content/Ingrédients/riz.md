# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | -------------------------------------- |
| Énergie                                                 | 333 kcal               |
| Matières grasses                                        | 1,29 g                                 |
| Acides gras saturés                                     | 0,281 g                                |
| Acides gras monoinsaturés                               | 0,358 g                                |
| Acides gras polyinsaturés                               | 0,392 g                                |
| Acides gras trans                                       | 0 g                                    |
| Cholestérol                                             | 0 mg                                   |
| Glucides                                                | 71,7 g                                 |
| Sucres                                                  | 0,42 g                                 |
| Fibres alimentaires                                     | 1,83 g                                 |
| Protéines                                               | 7,2 g                                  |
| Sel                                                     | 0,043 g                                |
| Alcool                                                  | 0 % vol                                |
| Vitamine B1 (Thiamine)                                  | 0,216 mg                               |
| Vitamine B3/PP (Niacine)                                | 5,3 mg                                 |
| Calcium                                                 | 17,7 mg                                |
| Phosphore                                               | 281 mg                                 |
| Fer                                                     | 1,73 mg                                |
| Magnésium                                               | 110 mg                                 |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %                                    |