# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 94 kcal    |
| Matières grasses     | 0,577 g    |
| Acides gras saturés  | 0,114 g    |
| Glucides             | 15,7 g     |
| Sucres               | 13,6 g     |
| Fibres alimentaires  | 3,14 g     |
| Protéines            | 4,46 g     |
| Sel                  | 0,699 g    |