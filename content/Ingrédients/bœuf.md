# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 171 kcal   |
| Matières grasses                                        | 9,14 g     |
| Acides gras saturés                                     | 3,68 g     |
| Glucides                                                | 0,864 g    |
| Sucres                                                  | 0,322 g    |
| Fibres alimentaires                                     | 0,265 g    |
| Protéines                                               | 19,9 g     |
| Sel                                                     | 0,762 g    |
| Alcool                                                  | 0 % vol    |
| Vitamine B12 (cobalamine)                               | 2,04 µg    |
| Fer                                                     | 2,92 mg    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0,267 %    |
| Rapport collagène sur protéines de viande (maximum)     | 14,5 %     |