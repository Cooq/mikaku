# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |48 kcal                  |
| Matières grasses     | 0,878 g                                |
| Acides gras saturés  | 0,114 g                                |
| Glucides             | 5,86 g                                 |
| Sucres               | 4,59 g                                 |
| Fibres alimentaires  | 1,54 g                                 |
| Protéines            | 1,62 g                                 |
| Sel                  | 0,409 g                                |