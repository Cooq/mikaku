
# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 59 kcal    |
| Matières grasses     | 2,99 g     |
| Acides gras saturés  | 0,266 g    |
| Glucides             | 5,83 g     |
| Sucres               | 4,59 g     |
| Fibres alimentaires  | 2,65 g     |
| Protéines            | 0,827 g    |
| Sel                  | 0,466 g    |
| Vitamine A (rétinol) | 1 140 µg   |

