# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 52 kcal    |
| Matières grasses     | 0.2 g      |
| Acides gras saturés  | 0 g        |
| Sel                  | 0,166 g    |
| Potassium            | 163 mg     |
| Glucides             | 0,7 g      |
| Fibres alimentaires  | 0 g        |
| Sucres               | 0,7 g      |
| Protéines            | 11 g       |