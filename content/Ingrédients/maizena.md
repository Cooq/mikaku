# Tableau Nutritionnel



| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | ---------- |
| Énergie                                                                                                      | 355 kcal |
| Matières grasses                                                                                             | 0,5 g      |
| Acides gras saturés                                                                                          | 0,1 g      |
| Glucides                                                                                                     | 86 g       |
| Sucres                                                                                                       | 0,5 g      |
| Fibres alimentaires                                                                                          | 1 g        |
| Protéines                                                                                                    | 0,5 g      |
| Sel                                                                                                          | 0,01 g     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0 %        |