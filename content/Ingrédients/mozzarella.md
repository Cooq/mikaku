# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 260 kcal |
| Matières grasses                                        | 20,8 g     |
| Acides gras saturés                                     | 14,2 g     |
| Glucides                                                | 0,951 g    |
| Sucres                                                  | 0,743 g    |
| Fibres alimentaires                                     | 0,038 g    |
| Protéines                                               | 16,9 g     |
| Sel                                                     | 0,659 g    |
| Calcium                                                 | 323 mg     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |