# Tableau Nutrionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 106 kcal   |
| Matières grasses     | 1 g        |
| Acides gras saturés  | 0,23 g     |
| Glucides             | 0,2 g      |
| Sucres               | 0,05 g     |
| Fibres alimentaires  | 0 g        |
| Protéines            | 22 g       |
| Sel                  | 0,13 g     |
