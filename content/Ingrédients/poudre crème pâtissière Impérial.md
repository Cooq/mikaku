# Tableau Nutritionnel



| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | -------------------------------------- |
| Énergie                                                                                                      | 355 kcal               |
| Matières grasses                                                                                             | 0,2 g                                  |
| Acides gras saturés                                                                                          | 0,1 g                                  |
| Glucides                                                                                                     | 88 g                                   |
| Sucres                                                                                                       | 0,1 g                                  |
| Fibres alimentaires                                                                                          | ?                                      |
| Protéines                                                                                                    | 0,3 g                                  |
| Sel                                                                                                          | 0,03 g                                 |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0 %                                    |