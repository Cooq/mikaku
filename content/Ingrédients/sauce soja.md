# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |104 kcal                 |
| Matières grasses     | 0,122 g                                |
| Acides gras saturés  | 0,026 g                                |
| Glucides             | 16,1 g                                 |
| Sucres               | 10,4 g                                 |
| Fibres alimentaires  | 0,455 g                                |
| Protéines            | 6,59 g                                 |
| Sel                  | 12,7 g                                 |