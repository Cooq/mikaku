# Tableau Nutritionnel
| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 247 kcal   |
| Matières grasses     | 1,2 g      |
| Acides gras saturés  | 0,3 g      |
| Glucides             | 80,6 g     |
| Sucres               | 2,2 g      |
| Fibres alimentaires  | 53,1 g     |
| Protéines            | 4 g        |

