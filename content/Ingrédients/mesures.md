
| Nom                                 | valeur en g |
| ----------------------------------- | ----------- |
| mg                                  | 0.001       |
| µg                                  | 0.000001    |
| kg                                  | 1000        |
| l                                   | 100         |
| ml                                  | 0.1         |
| cl                                  | 1           |
| g                                   | 1           |
| pincée                              | 0.4         |
| sachet                              | 7.5         |
| cuillère à café                     | 5           |
| cac                                 | 5           |
| cac bombée                          | 6           |
| cuillère à soupe                    | 15          |
| cas                                 | 15          |
| grosses cas                         | 17          |
| très grosse cas                     | 18          |
| œuf                                 | 55          |
| gros œuf                            | 65          |
| petit œuf                           | 45          |
| noix de beurre                      | 15          |
| gousse d’ail                        | 5           |
| grosse gousse d’ail                 | 8           |
| échalote                            | 25          |
| oignon                              | 80          |
| gros oignon                         | 100         |
| citron jaune                        | 120         |
| tomate                              | 130         |
| pomme de terre                      | 160         |
| orange                              | 200         |
| carotte                             | 100         |
| botte de persil                     | 50          |
| petite botte                        | 25          |
| poireau                             | 150         |
| courgette                           | 300         |
| blanc poulet                        | 180         |
| sel                                 | 0.4         |
| poivre                              | 0.4         |
| cuisse de canard confit en conserve | 380         |
| feuille de gélatine                 | 0.5         |
| jaune d'œuf                         | 20          |
| blanc d'œuf                         | 30          |
