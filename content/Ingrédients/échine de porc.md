# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 190 kcal   |
| Matières grasses     | 12,9 g     |
| Acides gras saturés  | 4,87 g     |
| Glucides             | 0,58 g     |
| Sucres               | 0,39 g     |
| Fibres alimentaires  | ?          |
| Protéines            | 17,6 g     |
| Sel                  | 0,524 g    |