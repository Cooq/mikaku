# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 401 kcal |
| Matières grasses     | 29,7 g     |
| Acides gras saturés  | 19,8 g     |
| Glucides             | 0,001 g    |
| Sucres               | 0 g        |
| Fibres alimentaires  | 0 g        |
| Protéines            | 32,2 g     |
| Sel                  | 1,6 g      |
| Calcium              | 1 160 mg   |
| Phosphore            | 685 mg     |