# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Pour 100 g |
| ----------------------------------------------------------------------------------------------------------- | ---------- |
| Énergie                                                                                                     | 859 kcal |
| Matières grasses                                                                                            | 95,5 g     |
| Acides gras saturés                                                                                         | 14,1 g     |
| Acides gras monoinsaturés                                                                                   | 72 g       |
| Acides gras polyinsaturés                                                                                   | 8,39 g     |
| Acides gras Oméga 3                                                                                         | 1 730 mg   |
| Acides gras Oméga 6                                                                                         | 6 570 mg   |
| Acides gras trans                                                                                           | 0 g        |
| Cholestérol                                                                                                 | 0 mg       |
| Glucides                                                                                                    | 0 g        |
| Sucres                                                                                                      | 0 g        |
| Fibres alimentaires                                                                                         | 0 g        |
| Protéines                                                                                                   | 0 g        |
| Sel                                                                                                         | 0 g        |
| Alcool                                                                                                      | 0 % vol    |
| Vitamine E (tocophérol)                                                                                     | 19,3 mg    |
| Potassium                                                                                                   | 0 mg       |
| Calcium                                                                                                     | 0 mg       |
| Fer                                                                                                         | 0 mg       |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 82,4 %     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 99,5 %     |