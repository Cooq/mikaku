# Tableau Nutritionnel

| Tableau nutritionnel      | Pour 100 g |
| ------------------------- | ---------- |
| Énergie                   | 721 kcal   |
| Matières grasses          | 79,7 g     |
| Acides gras saturés       | 53,5 g     |
| Acides gras monoinsaturés | 20,1 g     |
| Acides gras polyinsaturés | 7,78 g     |
| Acides gras Oméga 3       | 1 570 mg   |
| Glucides                  | 0,721 g    |
| Sucres                    | 0,618 g    |
| Fibres alimentaires       | 0,044 g    |
| Protéines                 | 0,677 g    |
| Sel                       | 0,893 g    |
| Alcool                    | 0 % vol    |
| Vitamine A (rétinol)      | 587 µg     |
| Vitamine D                | 5,68 µg    |
| Vitamine E (tocophérol)   | 3,46 mg    |

