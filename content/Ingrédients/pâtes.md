# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Tel que vendu  <br>pour 100 g / 100 ml |
| ----------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| Énergie                                                                                                     |322 kcal               |
| Matières grasses                                                                                            | 3,04 g                                 |
| Acides gras saturés                                                                                         | 0,945 g                                |
| Acides gras monoinsaturés                                                                                   | 1,7 g                                  |
| Acides gras polyinsaturés                                                                                   | 2,2 g                                  |
| Acides gras trans                                                                                           | 0 g                                    |
| Cholestérol                                                                                                 | 1,6 mg                                 |
| Glucides                                                                                                    | 59,5 g                                 |
| Sucres                                                                                                      | 2,62 g                                 |
| Sucres ajoutés                                                                                              | 1,79 g                                 |
| Amidon                                                                                                      | 58,2 g                                 |
| Fibres alimentaires                                                                                         | 3,18 g                                 |
| Protéines                                                                                                   | 10,9 g                                 |
| Sel                                                                                                         | 0,368 g                                |
| Alcool                                                                                                      | 0 % vol                                |
| Vitamine A (rétinol)                                                                                        | 22,3 µg                                |
| Vitamine D                                                                                                  | 0 µg                                   |
| Vitamine C (acide ascorbique)                                                                               | 0,491 mg                               |
| Vitamine B1 (Thiamine)                                                                                      | 0,51 mg                                |
| Vitamine B2 (Riboflavine)                                                                                   | 0,282 mg                               |
| Vitamine B3/PP (Niacine)                                                                                    | 4,04 mg                                |
| Vitamine B9 (Acide folique)                                                                                 | 110 µg                                 |
| Potassium                                                                                                   | 271 mg                                 |
| Calcium                                                                                                     | 42,6 mg                                |
| Phosphore                                                                                                   | 221 mg                                 |
| Fer                                                                                                         | 2,98 mg                                |
| Magnésium                                                                                                   | 75,5 mg                                |
| Zinc                                                                                                        | 2,5 mg                                 |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 1,17 %                                 |
| Fruits‚ légumes et noix - séchés                                                                            | 0 %                                    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 17,6 %                                 |
| Empreinte carbone                                                                                           | 137 g                                  |