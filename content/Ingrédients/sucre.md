# Tableau Nutritionnel

| Tableau nutritionnel                                    | Tel que vendu  <br>pour 100 g / 100 ml |
| ------------------------------------------------------- | -------------------------------------- |
| Énergie                                                 |386 kcal               |
| Matières grasses                                        | 0,112 g                                |
| Acides gras saturés                                     | 0,028 g                                |
| Glucides                                                | 97,3 g                                 |
| Sucres                                                  | 92,4 g                                 |
| Polyols                                                 | 76,3 g                                 |
| Fibres alimentaires                                     | 0,18 g                                 |
| Protéines                                               | 0,235 g                                |
| Sel                                                     | 0,038 g                                |
| Alcool                                                  | 0 % vol                                |
| Potassium                                               | 532 mg                                 |
| Calcium                                                 | 140 mg                                 |
| Fer                                                     | 4,87 mg                                |
| Magnésium                                               | 41,7 mg                                |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %                                    |