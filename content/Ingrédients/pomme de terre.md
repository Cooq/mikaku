# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 106 kcal   |
| Matières grasses                                        | 2,41 g     |
| Acides gras saturés                                     | 0,436 g    |
| Glucides                                                | 17,9 g     |
| Sucres                                                  | 0,624 g    |
| Fibres alimentaires                                     | 2,03 g     |
| Protéines                                               | 2,1 g      |
| Sel                                                     | 0,265 g    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 12,6 %     |