# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 45 kcal    |
| Matières grasses     | 1,76 g     |
| Acides gras saturés  | 0,408 g    |
| Glucides             | 7,7 g      |
| Sucres               | 0,261 g    |
| Fibres alimentaires  | 0,585 g    |
| Protéines            | 2,02 g     |
| Sel                  | 0,229 g    |