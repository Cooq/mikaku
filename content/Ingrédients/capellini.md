# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 359 kcal   |
| Matières grasses     | 2 g        |
| Acides gras saturés  | 0,5 g      |
| Glucides             | 71 g       |
| Sucres               | 3,5 g      |
| Fibres alimentaires  | 3 g        |
| Protéines            | 13 g       |
| Sel                  | 0,01 g     |
