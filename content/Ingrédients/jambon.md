# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 164 kcal   |
| Matières grasses                                        | 7,38 g     |
| Acides gras saturés                                     | 2,77 g     |
| Acides gras Oméga 3                                     | 82 mg      |
| Glucides                                                | 0,684 g    |
| Sucres                                                  | 0,524 g    |
| Fibres alimentaires                                     | 0,238 g    |
| Protéines                                               | 22,9 g     |
| Sel                                                     | 2,71 g     |
| Alcool                                                  | 0 % vol    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |