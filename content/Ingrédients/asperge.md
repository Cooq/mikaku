
# Tableau Nutritionnel
| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 19 kcal    |
| Matières grasses     | 0,29 g     |
| Acides gras saturés  | 0,058 g    |
| Glucides             | 1,89 g     |
| Sucres               | 1,13 g     |
| Fibres alimentaires  | 1,39 g     |
| Protéines            | 1,68 g     |
| Sel                  | 0,733 g    |

