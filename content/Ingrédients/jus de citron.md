# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Pour 100 g |
| ----------------------------------------------------------------------------------------------------------- | ---------- |
| Énergie                                                                                                     | 27 kcal    |
| Matières grasses                                                                                            | 0,152 g    |
| Acides gras saturés                                                                                         | 0,017 g    |
| Glucides                                                                                                    | 3,75 g     |
| Sucres                                                                                                      | 1,67 g     |
| Fibres alimentaires                                                                                         | 0,258 g    |
| Protéines                                                                                                   | 0,333 g    |
| Sel                                                                                                         | 0,007 g    |
| Vitamine C (acide ascorbique)                                                                               | 20,9 mg    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 72,8 %     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 92,3 %     |