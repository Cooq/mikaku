# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g  |
| ------------------------------------------------------- | ----------- |
| Énergie                                                 | 164 kcal    |
| Matières grasses                                        | 10,6 g      |
| Acides gras saturés                                     | 0,805 g     |
| Acides gras trans                                       | 0 g         |
| Cholestérol                                             | 0 mg        |
| Glucides                                                | 7,02 g      |
| Sucres                                                  | 4 g         |
| Fibres alimentaires                                     | 2,62 g      |
| Protéines                                               | 6,57 g      |
| Sel                                                     | 4,63 g      |
| Alcool                                                  | 0,083 % vol |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %         |