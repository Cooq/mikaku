# Tableau Nutritionnel

| Tableau nutritionnel                                    | Tel que vendu  <br>pour 100 g / 100 ml |
| ------------------------------------------------------- | -------------------------------------- |
| Énergie                                                 |225 kcal                 |
| Matières grasses                                        | 15,3 g                                 |
| Acides gras saturés                                     | 3,49 g                                 |
| Acides gras monoinsaturés                               | 6,95 g                                 |
| Acides gras polyinsaturés                               | 3,48 g                                 |
| Acides gras Oméga 3                                     | 2 010 mg                               |
| Acide eicosapentaénoïque                                | 0,723 g                                |
| Acide docosahexaénoïque                                 | 0,793 g                                |
| Acides gras trans                                       | 0,06 g                                 |
| Cholestérol                                             | 42 mg                                  |
| Glucides                                                | 0,568 g                                |
| Sucres                                                  | 0,296 g                                |
| Fibres alimentaires                                     | 0,159 g                                |
| Protéines                                               | 20,8 g                                 |
| Sel                                                     | 0,923 g                                |
| Alcool                                                  | 0 % vol                                |
| Vitamine A (rétinol)                                    | 12,4 µg                                |
| Vitamine D                                              | 6,63 µg                                |
| Vitamine C (acide ascorbique)                           | 0 mg                                   |
| Calcium                                                 | 343 mg                                 |
| Phosphore                                               | 364 mg                                 |
| Fer                                                     | 1,79 mg                                |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0,389 %                                |
