# Tableau Nutritionnel

Tableau de l'oignon traditionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 32 kcal    |
| Matières grasses     | 1,31 g     |
| Acides gras saturés  | 0,161 g    |
| Glucides             | 5,34 g     |
| Sucres               | 3,13 g     |
| Fibres alimentaires  | 1,83 g     |
| Protéines            | 1,39 g     |
| Sel                  | 0,296 g    |
