# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 26 kcal    |
| Matières grasses     | 1,97 g     |
| Acides gras saturés  | 0,201 g    |
| Glucides             | 1,67 g     |
| Sucres               | 0,422 g    |
| Fibres alimentaires  | ?          |
| Protéines            | 1,97 g     |
| Sel                  | ?          |
