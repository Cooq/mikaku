# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 19 kcal    |
| Matières grasses     | 0,041 g    |
| Acides gras saturés  | 0,009 g    |
| Glucides             | 3,25 g     |
| Sucres               | 2,98 g     |
| Fibres alimentaires  |            |
| Protéines            | 0,129 g    |
| Sel                  | 0,068 g    |