# Tableau Nutritionnel

| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | -------------------------------------- |
| Énergie                                                                                                      | 368 kcal               |
| Matières grasses                                                                                             | 4 g                                    |
| Acides gras saturés                                                                                          | 1 g                                    |
| Glucides                                                                                                     | 68 g                                   |
| Sucres                                                                                                       | 3 g                                    |
| Fibres alimentaires                                                                                          | 3 g                                    |
| Protéines                                                                                                    | 14 g                                   |
| Sel                                                                                                          | 0,08 g                                 |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                      | 0 %                                    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0 %                                    |