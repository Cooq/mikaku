# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 67 kcal    |
| Matières grasses                                        | 2,17 g     |
| Acides gras saturés                                     | 1,27 g     |
| Acides gras monoinsaturés                               | 0,763 g    |
| Acides gras polyinsaturés                               | 0,263 g    |
| Acides gras Oméga 3                                     | 58,2 mg    |
| Acide alpha-linolénique                                 | 0,145 g    |
| Acide linoléique                                        | 1,6 g      |
| Acides gras trans                                       | 0,027 g    |
| Cholestérol                                             | 7 mg       |
| Glucides                                                | 6,41 g     |
| Sucres                                                  | 5,73 g     |
| Lactose                                                 | 4,39 g     |
| Maltodextrines                                          | 0,345 g    |
| Fibres alimentaires                                     | 0,187 g    |
| Protéines                                               | 3,58 g     |
| Sel                                                     | 0,124 g    |
| Alcool                                                  | 0 % vol    |
| Vitamine A (rétinol)                                    | 212 µg     |
| Vitamine D                                              | 2,49 µg    |
| Vitamine E (tocophérol)                                 | 2,57 mg    |
| Vitamine K                                              | 13,3 µg    |
| Vitamine C (acide ascorbique)                           | 16,8 mg    |
| Vitamine B1 (Thiamine)                                  | 0,274 mg   |
| Vitamine B2 (Riboflavine)                               | 0,36 mg    |
| Vitamine B3/PP (Niacine)                                | 2,33 mg    |
| Vitamine B6 (Pyridoxine)                                | 0,232 mg   |
| Vitamine B9 (Acide folique)                             | 34,5 µg    |
| Vitamine B12 (cobalamine)                               | 1,34 µg    |
| Biotine (Vitamine B8 ou B7 ou H)                        | 6,45 µg    |
| Vitamine B5 (Acide pantothénique)                       | 1,31 mg    |
| Potassium                                               | 190 mg     |
| Chlorure                                                | 157 mg     |
| Calcium                                                 | 127 mg     |
| Phosphore                                               | 125 mg     |
| Fer                                                     | 1,18 mg    |
| Magnésium                                               | 34,5 mg    |
| Zinc                                                    | 1,5 mg     |
| Cuivre                                                  | 1,33 mg    |
| Manganèse                                               | 0,819 mg   |
| Fluorure                                                | 5,73 mg    |
| Sélénium                                                | 8,91 µg    |
| Iode                                                    | 31,4 µg    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |