# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 101 kcal   |
| Matières grasses     | 0,398 g    |
| Acides gras saturés  | 0,1 g      |
| Glucides             | 18,1 g     |
| Sucres               | 0,936 g    |
| Fibres alimentaires  | 0,2 g      |
| Protéines            | 1,56 g     |
| Sel                  | 12,6 g     |