# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 478 kcal   |
| Matières grasses     | 19 g       |
| Acides gras saturés  | 11,5 g     |
| Glucides             | 69,5 g     |
| Sucres               | 43 g       |
| Fibres alimentaires  | 2,17 g     |
| Protéines            | 5,9 g      |
| Sel                  | 0,651 g    |
