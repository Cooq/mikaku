# Tableau Nutritionnel

| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | ---------- |
| Énergie                                                                                                      | 240 kcal |
| Matières grasses                                                                                             | 0,1 g      |
| Acides gras saturés                                                                                          | 0 g        |
| Glucides                                                                                                     | 59 g       |
| Sucres                                                                                                       | 59 g       |
| Fibres alimentaires                                                                                          | 0,7 g      |
| Protéines                                                                                                    | 0,3 g      |
| Sel                                                                                                          | 0 g        |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                      | 28 %       |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0 %        |