# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 291 kcal |
| Matières grasses     | 29,7 g     |
| Acides gras saturés  | 20,1 g     |
| Glucides             | 3,31 g     |
| Sucres               | 2,88 g     |
| Fibres alimentaires  | 0,104 g    |
| Protéines            | 2,37 g     |
| Sel                  | 0,086 g    |