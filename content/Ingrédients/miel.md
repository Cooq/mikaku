# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 297 kcal |
| Matières grasses                                        | 0,106 g    |
| Acides gras saturés                                     | 0,028 g    |
| Glucides                                                | 78,5 g     |
| Sucres                                                  | 72,3 g     |
| Fibres alimentaires                                     | 0,057 g    |
| Protéines                                               | 0,346 g    |
| Sel                                                     | 0,016 g    |
| Alcool                                                  | 0 % vol    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |