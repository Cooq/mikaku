# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g             |
| -------------------- | ---------------------- |
| Énergie              |164 kcal |
| Matières grasses     | 16,3 g                 |
| Acides gras saturés  | 14,3 g                 |
| Acides gras trans    | 0 g                    |
| Cholestérol          | 0 mg                   |
| Glucides             | 2,72 g                 |
| Sucres               | 1,7 g                  |
| Fibres alimentaires  | 0,406 g                |
| Protéines            | 1,31 g                 |
| Sel                  | 0,069 g                |