
# Tableau Nutritionnel
| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 118 kcal   |
| Matières grasses     | 8,72 g     |
| Acides gras saturés  | 1,18 g     |
| Glucides             | 6,21 g     |
| Sucres               | 3,89 g     |
| Fibres alimentaires  | 2,29 g     |
| Protéines            | 1,48 g     |
| Sel                  | 0,77 g     |
