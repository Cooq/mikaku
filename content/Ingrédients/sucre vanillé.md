# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |394 kcal               |
| Matières grasses     | 0,173 g                                |
| Acides gras saturés  | 0,044 g                                |
| Glucides             | 98,7 g                                 |
| Sucres               | 97,4 g                                 |
| Fibres alimentaires  | 0,156 g                                |
| Protéines            | 0,183 g                                |
| Sel                  | 0,006 g                                |