# Tableau Nutritionnel

  

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 396 kcal   |
| Matières grasses     | 0 g        |
| Acides gras saturés  | 0 g        |
| Glucides             | 99 g       |
| Sucres               | 99 g       |
| Fibres alimentaires  | 0 g        |
| Protéines            | 0 g        |
| Sel                  | 0 g        |
