# Tableau Nutritionnel



| Tableau nutritionnel     | Tel que vendu  <br>pour 100 g / 100 ml |
| ------------------------ | -------------------------------------- |
| Énergie                  | 485 kcal                                |
| Matières grasses         | 25,9 g                                 |
| Acides gras saturés      | 2,2 g                                  |
| Graisses mono-insaturées | 14,7 g                                 |
| Graisses polyinsaturées  | 7,8 g                                  |
| Glucides                 | 59,6 g                                 |
| Sucres                   | 55,8 g                                 |
| Fibres alimentaires      | 3,5 g                                  |
| Protéines                | 3,3 g                                  |
| Sel                      | 0,03 g                                 |

