# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 35 kcal    |
| Matières grasses     | 1,23 g     |
| Acides gras saturés  | 0,164 g    |
| Glucides             | 4,21 g     |
| Sucres               | 2,08 g     |
| Fibres alimentaires  | 2,6 g      |
| Protéines            | 1,38 g     |
| Sel                  | 0,029 g    |