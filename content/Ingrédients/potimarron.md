# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              | 38 kcal<br>                            |
| Glucides             | 6,88 g                                 |
| Protéines            | 1 g                                    |
| Matières grasses     | 0,3 g                                  |
| Fibres alimentaires  | 2,03 g                                 |
| Acides gras saturés  | 1,8 g                                  |
