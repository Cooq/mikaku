# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Pour 100 g |
| ----------------------------------------------------------------------------------------------------------- | ---------- |
| Énergie                                                                                                     | 189 kcal   |
| Matières grasses                                                                                            | 18,4 g     |
| Acides gras saturés                                                                                         | 2,79 g     |
| Acides gras Oméga 3                                                                                         | 1 380 mg   |
| Acides gras Oméga 6                                                                                         | 3 700 mg   |
| Acides gras trans                                                                                           | 0 g        |
| Glucides                                                                                                    | 2,02 g     |
| Sucres                                                                                                      | 0,282 g    |
| Fibres alimentaires                                                                                         | 3,45 g     |
| Protéines                                                                                                   | 1,33 g     |
| Sel                                                                                                         | 3,46 g     |
| Alcool                                                                                                      | 0 % vol    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 79,2 %     |
| Fruits‚ légumes et noix - séchés                                                                            | 0 %        |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 96,7 %     |