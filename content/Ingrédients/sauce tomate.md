# Tableau Nutritionnel


| Tableau nutritionnel                                                                                        | Tel que vendu  <br>pour 100 g / 100 ml |
| ----------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| Énergie                                                                                                     |79 kcal                  |
| Matières grasses                                                                                            | 2,09 g                                 |
| Acides gras saturés                                                                                         | 0,344 g                                |
| Acides gras trans                                                                                           | 0 g                                    |
| Cholestérol                                                                                                 | 0,056 mg                               |
| Glucides                                                                                                    | 11 g                                   |
| Sucres                                                                                                      | 8,97 g                                 |
| Fibres alimentaires                                                                                         | 1,54 g                                 |
| Protéines                                                                                                   | 1,55 g                                 |
| Sel                                                                                                         | 1,19 g                                 |
| Alcool                                                                                                      | 0 % vol                                |
| Vitamine A (rétinol)                                                                                        | 125 µg                                 |
| Vitamine C (acide ascorbique)                                                                               | 7,06 mg                                |
| Potassium                                                                                                   | 379 mg                                 |
| Calcium                                                                                                     | 31,7 mg                                |
| Fer                                                                                                         | 0,837 mg                               |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 69,2 %                                 |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 67,8 %                                 |