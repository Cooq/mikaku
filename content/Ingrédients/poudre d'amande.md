# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 622 kcal   |
| Matières grasses     | 53,1 g     |
| Acides gras saturés  | 4,18 g     |
| Glucides             | 9,26 g     |
| Sucres               | 4,31 g     |
| Fibres alimentaires  | 9,16 g     |
| Protéines            | 23,4 g     |
| Sel                  | 0,025 g    |
