# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | -------------------------------------- |
| Énergie              | 130 kcal                 |
| Matières grasses     | 2,38 g                                 |
| Acides gras saturés  | 1,68 g                                 |
| Glucides             | 12,1 g                                 |
| Sucres               | 9,43 g                                 |
| Fibres alimentaires  | 0,05 g                                 |
| Protéines            | 3,68 g                                 |
| Sel                  | 1,25 g                                 |
| Alcool               | 40,8 % vol                             |
