# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 33 kcal    |
| Matières grasses     | 1,62 g     |
| Acides gras saturés  | 0,17 g     |
| Glucides             | 3,6 g      |
| Sucres               | 1,01 g     |
| Protéines            | 2,92 g     |
| Sel                  | 0,065 g    |