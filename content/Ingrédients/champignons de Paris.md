
# Tableau Nutritionnel
  

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 22 kcal    |
| Matières grasses     | 0 g        |
| Acides gras saturés  | 0 g        |
| Glucides             | 2,7 g      |
| Sucres               | 0 g        |
| Fibres alimentaires  | 1,3 g      |
| Protéines            | 1,8 g      |
| Sel                  | 0 g        |
