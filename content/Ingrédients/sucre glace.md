Du [[sucre]] mais broyé en plus fin

# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |383 kcal               |
| Matières grasses     | 0,002 g                                |
| Acides gras saturés  | 0,002 g                                |
| Glucides             | 99,5 g                                 |
| Sucres               | 95,7 g                                 |
| Fibres alimentaires  | 0,01 g                                 |
| Protéines            | 0,014 g                                |
| Sel                  | 0,002 g                                |