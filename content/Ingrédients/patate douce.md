# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 84 kcal    |
| Matières grasses     | 1,41 g     |
| Acides gras saturés  | 0,483 g    |
| Glucides             | 15 g       |
| Sucres               | 5,49 g     |
| Fibres alimentaires  | 2,24 g     |
| Protéines            | 1,41 g     |
| Sel                  | 0,23 g     |