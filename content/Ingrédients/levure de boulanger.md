# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 163 kcal   |
| Matières grasses     | 3,22 g     |
| Acides gras saturés  | 1,34 g     |
| Glucides             | 15,4 g     |
| Sucres               | 1,56 g     |
| Fibres alimentaires  | 12,6 g     |
| Protéines            | 18,3 g     |
| Sel                  | 0,307 g    |