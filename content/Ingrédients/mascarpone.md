# Tableau Nutritionnel
| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 382 kcal |
| Matières grasses     | 38,9 g     |
| Acides gras saturés  | 26,7 g     |
| Glucides             | 3,18 g     |
| Sucres               | 3,05 g     |
| Fibres alimentaires  | 0,064 g    |
| Protéines            | 4,89 g     |
| Sel                  | 0,103 g    |
