# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |360 kcal               |
| Matières grasses     | 7,1 g                                  |
| Acides gras saturés  | 1,24 g                                 |
| Glucides             | 52 g                                   |
| Sucres               | 1,5 g                                  |
| Fibres alimentaires  | 13,8 g                                 |
| Protéines            | 14,7 g                                 |
| Sel                  | 0,024 g                                |