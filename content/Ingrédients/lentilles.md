# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g               |
| ------------------------------------------------------- | ------------------------ |
| Énergie                                                 |281 kcal |
| Matières grasses                                        | 1,33 g                   |
| Acides gras saturés                                     | 0,2 g                    |
| Glucides                                                | 38,2 g                   |
| Sucres                                                  | 1,25 g                   |
| Fibres alimentaires                                     | 15 g                     |
| Protéines                                               | 21,4 g                   |
| Sel                                                     | 0,141 g                  |
| Phosphore                                               | 349 mg                   |
| Fer                                                     | 6,9 mg                   |
| Magnésium                                               | 98,6 mg                  |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 69,4 %                   |