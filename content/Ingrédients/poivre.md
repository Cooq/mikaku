# Tableau Nutritionnel

Tableau nutritionnel du poivre

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |97 kcal                  |
| Matières grasses     | 1,94 g                                 |
| Acides gras saturés  | 0,527 g                                |
| Glucides             | 16,1 g                                 |
| Sucres               | 0,666 g                                |
| Fibres alimentaires  | 12,5 g                                 |
| Protéines            | 4,33 g                                 |
| Sel                  | 0,652 g                                |
