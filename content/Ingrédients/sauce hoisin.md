# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | -------------------------------------- |
| Énergie              | 207 kcal                 |
| Matières grasses     | 1,89 g                                 |
| Acides gras saturés  | 0,231 g                                |
| Glucides             | 42,1 g                                 |
| Sucres               | 38,4 g                                 |
| Fibres alimentaires  | ?                                      |
| Protéines            | 2,16 g                                 |
| Sel                  | 4,87 g                                 |