# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 396 kcal   |
| Matières grasses     | 5,45 g     |
| Acides gras saturés  | 3,14 g     |
| Glucides             | 86,5 g     |
| Sucres               | 82,7 g     |
| Fibres alimentaires  | 0,304 g    |
| Protéines            | 0,077 g    |
| Sel                  | 0,107 g    |