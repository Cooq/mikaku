# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 96 kcal    |
| Matières grasses     | 0,119 g    |
| Acides gras saturés  | 0,019 g    |
| Glucides             | 20,8 g     |
| Sucres               | 15,3 g     |
| Fibres alimentaires  | 0,065 g    |
| Protéines            | 1,86 g     |
| Sel                  | 9 g        |
