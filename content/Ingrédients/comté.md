# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 420 kcal   |
| Matières grasses     | 34,8 g     |
| Acides gras saturés  | 22,8 g     |
| Glucides             | 0,433 g    |
| Sucres               | 0,239 g    |
| Fibres alimentaires  | 0,042 g    |
| Protéines            | 26,9 g     |
| Sel                  | 0,845 g    |
| Calcium              | 906 mg     |