# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 367 kcal   |
| Matières grasses     | 3,49 g     |
| Acides gras saturés  | 0,724 g    |
| Glucides             | 71,7 g     |
| Sucres               | 4,1 g      |
| Fibres alimentaires  | 3,83 g     |
| Protéines            | 10,9 g     |
| Sel                  | 1,29 g     |