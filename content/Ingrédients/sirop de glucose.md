# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 314 kcal   |
| Glucides             | 79 g       |
| Sucres               | 28 g       |
| Sel                  | 0,01 g     |
