# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 139 kcal   |
| Matières grasses                                        | 9,71 g     |
| Acides gras saturés                                     | 2,59 g     |
| Acides gras Oméga 3                                     | 313 mg     |
| Glucides                                                | 0,523 g    |
| Sucres                                                  | 0,392 g    |
| Fibres alimentaires                                     | 0,074 g    |
| Protéines                                               | 12,5 g     |
| Sel                                                     | 0,307 g    |
| Vitamine B12 (cobalamine)                               | 1,45 µg    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |