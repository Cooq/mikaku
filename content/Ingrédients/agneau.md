
# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 144 kcal   |
| Matières grasses     | 6,64 g     |
| Acides gras saturés  | 2,58 g     |
| Glucides             | 1,36 g     |
| Sucres               | 0,487 g    |
| Fibres alimentaires  | 0,284 g    |
| Protéines            | 18,9 g     |
| Sel                  | 0,901 g    |
