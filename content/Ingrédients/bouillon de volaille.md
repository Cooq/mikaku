# Tableau Nutritionnel

| Tableau nutritionnel | Préparé  <br>pour 100 ml |
| -------------------- | ------------------------ |
| Énergie              | 5 kcal                   |
| Matières grasses     | 0,5 g                    |
| Acides gras saturés  | 0,1 g                    |
| Glucides             | 0,6 g                    |
| Sucres               | 0,5 g                    |
| Fibres alimentaires  | 0,5 g                    |
| Protéines            | 0,5 g                    |
| Sel                  | 1 g                      |

