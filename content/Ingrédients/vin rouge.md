# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 32 kcal    |
| Matières grasses     | 1,02 g     |
| Acides gras saturés  | 0,253 g    |
| Glucides             | 3,62 g     |
| Sucres               | 1,36 g     |
| Fibres alimentaires  | 0,278 g    |
| Protéines            | 0,488 g    |
| Sel                  | 0,188 g    |
| Alcool               | 13,1 % vol |
