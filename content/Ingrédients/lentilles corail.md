# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |332 kcal               |
| Matières grasses     | 1,54 g                                 |
| Acides gras saturés  | 0,255 g                                |
| Glucides             | 48,3 g                                 |
| Sucres               | 1,66 g                                 |
| Fibres alimentaires  | 12,5 g                                 |
| Protéines            | 24,7 g                                 |
| Sel                  | 0,04 g                                 |