# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 161 kcal   |
| Matières grasses     | 0,87 g     |
| Acides gras saturés  | 0,151 g    |
| Glucides             | 21,5 g     |
| Sucres               | 1,49 g     |
| Fibres alimentaires  | 9,02 g     |
| Protéines            | 11,2 g     |
| Sel                  | 0,38 g     |
