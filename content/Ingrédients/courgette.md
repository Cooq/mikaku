# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 27 kcal    |
| Matières grasses     | 0,891 g    |
| Acides gras saturés  | 0,159 g    |
| Glucides             | 2,67 g     |
| Sucres               | 1,7 g      |
| Fibres alimentaires  | 1,25 g     |
| Protéines            | 1,42 g     |
| Sel                  | 0,186 g    |