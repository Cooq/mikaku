# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |188 kcal                 |
| Matières grasses     | 11,5 g                                 |
| Acides gras saturés  | 1,96 g                                 |
| Glucides             | 12,9 g                                 |
| Sucres               | 5,14 g                                 |
| Fibres alimentaires  | 5,22 g                                 |
| Protéines            | 3,53 g                                 |
| Sel                  | 6,72 g                                 |