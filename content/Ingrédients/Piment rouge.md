# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 100 kcal   |
| Matières grasses     | 6,32 g     |
| Acides gras saturés  | 1,06 g     |
| Glucides             | 7,89 g     |
| Sucres               | 2,83 g     |
| Fibres alimentaires  | ?          |
| Protéines            | 2,57 g     |
| Sel                  | 5,19 g     |