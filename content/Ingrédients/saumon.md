# Tableau Nutritionnel

| Tableau nutritionnel                                    | Tel que vendu  <br>pour 100 g / 100 ml |
| ------------------------------------------------------- | -------------------------------------- |
| Énergie                                                 |186 kcal                 |
| Matières grasses                                        | 10,7 g                                 |
| Acides gras saturés                                     | 2,04 g                                 |
| Acides gras monoinsaturés                               | 4,61 g                                 |
| Acides gras polyinsaturés                               | 3,01 g                                 |
| Acides gras Oméga 3                                     | 1 590 mg                               |
| Acides gras Oméga 6                                     | 966 mg                                 |
| Acide eicosapentaénoïque                                | 0,752 g                                |
| Acide docosahexaénoïque                                 | 0,537 g                                |
| Glucides                                                | 0,552 g                                |
| Sucres                                                  | 0,32 g                                 |
| Fibres alimentaires                                     | 0,223 g                                |
| Protéines                                               | 21,5 g                                 |
| Sel                                                     | 2,27 g                                 |
| Alcool                                                  | 0 % vol                                |
| Vitamine D                                              | 3,81 µg                                |
| Vitamine E (tocophérol)                                 | 1,66 mg                                |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 1,06 %                                 |