# Tableau Nutritionnel
Tableau nutritionnel de l'huile de tournesol

| Tableau nutritionnel      | Pour 100 g |
| ------------------------- | ---------- |
| Énergie                   | 874 kcal |
| Matières grasses          | 97 g       |
| Acides gras saturés       | 10,5 g     |
| Acides gras monoinsaturés | 33,5 g     |
| Acides gras polyinsaturés | 51,9 g     |
| Acides gras Oméga 6       | 42 100 mg  |
| Glucides                  | 0,009 g    |
| Sucres                    | 0,008 g    |
| Fibres alimentaires       | 0 g        |
| Protéines                 | 0,011 g    |
| Sel                       | 0 g        |
| Vitamine E (tocophérol)   | 56,3 mg    |