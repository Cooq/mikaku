
# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 26 kcal    |
| Matières grasses     | 0,45 g     |
| Acides gras saturés  | 0,099 g    |
| Glucides             | 2,41 g     |
| Sucres               | 1,64 g     |
| Fibres alimentaires  | 2,11 g     |
| Protéines            | 2,04 g     |
| Sel                  | 0,04 g     |

