# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 64 kcal    |
| Matières grasses     | 0,291 g    |
| Acides gras saturés  | 0,032 g    |
| Glucides             | 11,8 g     |
| Sucres               | 9,85 g     |
| Fibres alimentaires  | 1,5 g      |
| Protéines            | 0,832 g    |
| Sel                  | 0,017 g    |