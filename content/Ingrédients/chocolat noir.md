# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Pour 100 g |
| ----------------------------------------------------------------------------------------------------------- | ---------- |
| Énergie                                                                                                     | 557 kcal   |
| Matières grasses                                                                                            | 39,5 g     |
| Acides gras saturés                                                                                         | 23,2 g     |
| Acides gras monoinsaturés                                                                                   | 13,8 g     |
| Acides gras polyinsaturés                                                                                   | 1,24 g     |
| Acides gras trans                                                                                           | 0 g        |
| Cholestérol                                                                                                 | 1,77 mg    |
| Glucides                                                                                                    | 38,2 g     |
| Sucres                                                                                                      | 31,3 g     |
| Sucres ajoutés                                                                                              | 23,4 g     |
| Polyols                                                                                                     | 27,9 g     |
| Fibres alimentaires                                                                                         | 9,15 g     |
| Protéines                                                                                                   | 7,69 g     |
| Sel                                                                                                         | 0,053 g    |
| Alcool                                                                                                      | 0 % vol    |
| Vitamine A (rétinol)                                                                                        | 14,2 µg    |
| Vitamine D                                                                                                  | 1,03 µg    |
| Vitamine C (acide ascorbique)                                                                               | 0 mg       |
| Potassium                                                                                                   | 588 mg     |
| Calcium                                                                                                     | 50,5 mg    |
| Phosphore                                                                                                   | 287 mg     |
| Fer                                                                                                         | 9,65 mg    |
| Magnésium                                                                                                   | 162 mg     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 3,53 %     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 13,8 %     |
| Cacao (minimum)                                                                                             | 67,4 %     |
| Empreinte carbone                                                                                           | 188 g      |