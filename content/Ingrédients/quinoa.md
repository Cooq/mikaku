# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |324 kcal               |
| Matières grasses     | 5,3 g                                  |
| Acides gras saturés  | 0,643 g                                |
| Glucides             | 53,7 g                                 |
| Sucres               | 2,47 g                                 |
| Fibres alimentaires  | 6,52 g                                 |
| Protéines            | 11,5 g                                 |
| Sel                  | 0,172 g                                |
| Calcium              | 32,6 mg                                |
| Fer                  | 7,94 mg                                |
| Magnésium            | 133 mg                                 |