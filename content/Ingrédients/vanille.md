# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |179 kcal                 |
| Matières grasses     | 3,85 g                                 |
| Acides gras saturés  | 1,33 g                                 |
| Glucides             | 30,8 g                                 |
| Sucres               | 19,1 g                                 |
| Fibres alimentaires  | 4,51 g                                 |
| Protéines            | 1,9 g                                  |
| Sel                  | 0,119 g                                |