# Tableau Nutritionnel

| Tableau nutritionnel                                    | Pour 100 g |
| ------------------------------------------------------- | ---------- |
| Énergie                                                 | 241 kcal |
| Matières grasses                                        | 9,51 g     |
| Acides gras saturés                                     | 1,57 g     |
| Glucides                                                | 26,1 g     |
| Sucres                                                  | 3,18 g     |
| Fibres alimentaires                                     | 6,5 g      |
| Protéines                                               | 9,27 g     |
| Sel                                                     | 3,55 g     |
| Alcool                                                  | 0 % vol    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive | 0 %        |
| Fruits‚ légumes et noix - séchés                        | 0 %        |