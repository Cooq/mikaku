# Tableau Nutritionnel

| Tableau nutritionnel                                                                                         | Pour 100 g |
| ------------------------------------------------------------------------------------------------------------ | ---------- |
| Énergie                                                                                                      | 55 kcal    |
| Matières grasses                                                                                             | < 0,5 g    |
| Acides gras saturés                                                                                          | < 0,1 g    |
| Glucides                                                                                                     | 3,4 g      |
| Sucres                                                                                                       | 3,4 g      |
| Fibres alimentaires                                                                                          | 0 g        |
| Protéines                                                                                                    | 10 g       |
| Sel                                                                                                          | 24,4 g     |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation par analyse de la liste des ingrédients) | 0 %        |