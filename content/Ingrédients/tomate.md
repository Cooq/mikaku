# Tableau Nutritionnel

| Tableau nutritionnel                                                                                        | Tel que vendu  <br>pour 100 g / 100 ml |
| ----------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| Énergie                                                                                                     |32 kcal                  |
| Matières grasses                                                                                            | 0,53 g                                 |
| Acides gras saturés                                                                                         | 0,097 g                                |
| Acides gras trans                                                                                           | 0 g                                    |
| Cholestérol                                                                                                 | 0 mg                                   |
| Glucides                                                                                                    | 4,18 g                                 |
| Sucres                                                                                                      | 3,27 g                                 |
| Fibres alimentaires                                                                                         | 1,42 g                                 |
| Protéines                                                                                                   | 1,33 g                                 |
| Sel                                                                                                         | 0,282 g                                |
| Alcool                                                                                                      | 0 % vol                                |
| Vitamine C (acide ascorbique)                                                                               | 14,6 mg                                |
| Calcium                                                                                                     | 19,5 mg                                |
| Fer                                                                                                         | 0,591 mg                               |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive                                                     | 58 %                                   |
| Fruits‚ légumes et noix - séchés                                                                            | 0 %                                    |
| Fruits‚ légumes‚ noix et huiles de colza‚ noix et olive (estimation manuelle avec la liste des ingrédients) | 84,3 %                                 |