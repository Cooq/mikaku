# Tableau Nutritionnel

| Tableau nutritionnel | Tel que vendu  <br>pour 100 g / 100 ml |
| -------------------- | -------------------------------------- |
| Énergie              |22 kcal                   |
| Matières grasses     | 0,52 g                                 |
| Acides gras saturés  | 0,1 g                                  |
| Glucides             | 1,68 g                                 |
| Sucres               | 0,205 g                                |
| Fibres alimentaires  | ?                                      |
| Protéines            | 0,52 g                                 |
| Sel                  | 0,001 g                                |