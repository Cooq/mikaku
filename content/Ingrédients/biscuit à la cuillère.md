# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 357 kcal   |
| Matières grasses     | 3,6 g      |
| Acides gras saturés  | 1,4 g      |
| Glucides             | 73 g       |
| Sucres               | 48 g       |
| Fibres alimentaires  | 0 g        |
| Protéines            | 7,6 g      |
| Sel                  | 0,47 g     |

