# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 322 kcal   |
| Matières grasses     | 27 g       |
| Acides gras saturés  | 10 g       |
| Glucides             | 3.6 g      |
| Sucres               | 0.6 g      |
| Fibres alimentaires  | 0 g        |
| Protéines            | 16 g       |
| Sel                  | 0,048 g    |