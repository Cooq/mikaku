# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 93 kcal    |
| Matières grasses     | 7,31 g     |
| Acides gras saturés  | 0,81 g     |
| Glucides             | 12,3 g     |
| Sucres               | 3,04 g     |
| Fibres alimentaires  | ?          |
| Protéines            | 5,41 g     |
| Sel                  | 0,578 g    |