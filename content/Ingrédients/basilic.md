# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 73 kcal    |
| Matières grasses     | 4,73 g     |
| Acides gras saturés  | 0,888 g    |
| Glucides             | 3,11 g     |
| Sucres               | 0,829 g    |
| Fibres alimentaires  | 4,48 g     |
| Protéines            | 3,07 g     |
| Sel                  | 0,63 g     |