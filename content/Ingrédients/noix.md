# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 675 kcal |
| Matières grasses     | 62,5 g     |
| Acides gras saturés  | 5,9 g      |
| Acides gras Oméga 3  | 6 380 mg   |
| Glucides             | 10,5 g     |
| Sucres               | 3,72 g     |
| Fibres alimentaires  | 6,22 g     |
| Protéines            | 14,9 g     |
| Sel                  | 0,022 g    |
| Magnésium            | 148 mg     |