# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 ml |
| -------------------- | ----------- |
| Énergie              | 3 kcal      |
| Matières grasses     | 0,034 g     |
| Acides gras saturés  | 0,02 g      |
| Glucides             | 0,54 g      |
| Sucres               | 0,124 g     |
| Fibres alimentaires  | 0,007 g     |
| Protéines            | 0,2 g       |
| Sel                  | 0,005 g     |
