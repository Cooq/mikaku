# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 720 kcal   |
| Matières grasses     | 79,4 g     |
| Acides gras saturés  | 54,3 g     |
| Glucides             | 0,732 g    |
| Sucres               | 0,628 g    |
| Fibres alimentaires  | 0,037 g    |
| Protéines            | 0,691 g    |
| Sel                  | 2,05 g     |
| Vitamine A (rétinol) | 570 µg     |
