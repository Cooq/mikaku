# Tableau Nutritionnel

| Tableau nutritionnel | Pour 100 g |
| -------------------- | ---------- |
| Énergie              | 108 kcal   |
| Matières grasses     | 2,37 g     |
| Acides gras saturés  | 0,493 g    |
| Glucides             | 1,8 g      |
| Sucres               | 0,425 g    |
| Fibres alimentaires  | 0,377 g    |
| Protéines            | 17,8 g     |
| Sel                  | 1,44 g     |