---
title: Bienvenue
ignoreInList: true
---

![[logo-wide.svg]]

# Introduction
---
Bienvenue sur recettes.coquery.fr nouvelle version, plus complète et plus simple à maintenir pour moi. Toute contribution est la bienvenue, pour cela il faudra m'envoyer la recette sous format texte et je m'occuperai de la reformater.
J'ai essayé de rendre la navigation simple et les instructions claires. En espérant que les saveurs que vous y trouverez seront à votre goût. Mais sans plus tarder, prenez votre tablier et préparons ensemble des repas qui réconfortent le cœur et l’estomac !

# Navigation
---
## Tags
Les recettes ont toutes des tags qui permettent de les rassembler dans des pages
Par exemple, vous pouvez retrouver les recettes de #Plat ou #Dessert #Français

Vous pouvez voir l'ensemble des tags sur l'url [/tags/](/tags/)
## Dossiers

Les recettes sont aussi classées par dossier. Vous pouvez aussi voir la liste des [recettes sucrées](/Recettes/Sucré)
et des [recettes salées](/Recettes/Salé)

# Nutrition
---
## Tableaux nutritionnels
Tous, ou presque, les tableaux nutritionnels viennent de [openfoodfacts.org](https://fr.openfoodfacts.org). Pour rechercher un nouveau tableau, il suffit de mettre le nom de l'ingrédient dans l'url suivant : 
https://fr.openfoodfacts.org/categorie/NOM_INGREDIENT
Le site est assez indulgent quant à la façon de l'écrire et redirigera vers la page la plus proche.

*Par exemple : pour du beurre demi-sel, rendez-vous sur la page https://fr.openfoodfacts.org/categorie/beurres-demi-sel*

## Calculs
Le calcul du tableau nutritionnel se fait donc automatiquement en fonction des ingrédients. Ces résultats sont une approximation

# Design
---
La police de caractère des titres est `Gloria Hallelujah`, celle du texte est `Inter`  et `IBM Plex Mono` pour ce genre de citation

Les graphiques sont créés à la main en se basant sur les icônes de [khushmeen](https://khushmeen.com/icons.html)