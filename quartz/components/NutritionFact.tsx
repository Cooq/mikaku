import { QuartzComponentConstructor } from "./types"
// @ts-ignore
import script from "./scripts/nutritionfact.inline"
import style from "./styles/nutritionfact.scss"

export default (() => {
  function NutritionFact() {
    return <div id="nutrition-facts" className="table-container hidden">
      <h3>Valeurs nutritionnelles</h3>
      <table>
        <thead>
          <th>Catégorie</th>
          <th id="portion">Pour</th>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  }

  NutritionFact.afterDOMLoaded = script;
  NutritionFact.css = style;

  return NutritionFact
}) satisfies QuartzComponentConstructor