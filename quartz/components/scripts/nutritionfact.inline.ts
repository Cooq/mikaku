type NutritionValue = {
  unit: string
  quantity: number
}

/**
 * Get the empty values for the nutrition facts
 * @returns {object} The empty values
 */
function getEmptyValues(): { [key: string]: NutritionValue | null } {
  return {
    "Énergie": null,
    "Matières grasses": null,
    "Acides gras saturés": null,
    "Acides gras trans": null,
    "Glucides": null,
    "Sucres": null,
    "Fibres alimentaires": null,
    "Protéines": null,
    "Sel": null,
  };
}

/**
 * Check if the current page is a recipe page
 * @returns {boolean} true if the current page is a recipe page
 */
function isRecipePage(): boolean {
  return window.location.pathname.includes('Recettes');
}

/**
 * Delete the nutrition fact table from the page
 */
function deleteNutritionFact() {
  document.getElementById('nutrition-facts')?.remove();
}

/**
 * Parse the ingredient to extract the nutrition facts
 * @param {string} content - The ingredient to parse
 * @returns {object} The nutrition facts
 */
function parseIngredient(content: string): { [key: string]: NutritionValue | null } {
  let values = getEmptyValues();

  for (const name in values) {
    const regex = new RegExp(`${name}(?<quantity>\\d+(?:,\\d+)?) (?<unit>[a-zµ]*)`, '');
    const match = content.match(regex)?.groups;
    if (match) {
      values[name] = {
        unit: match.unit,
        quantity: parseFloat(match.quantity.replace(',', '.'))
      };
    }
  }
  return values;
}

/**
 * Parse the measures from the content
 * @param {string} content - The content to parse
 * @returns {object} The measures
 */
function parseMeasures(content: string): { [key: string]: string } {
  const measures: { [key: string]: string } = {};
  content = content.replace(/.+valeur en g/, '');
  const matches = content.matchAll(/(?<name>[^0-9]+)(?<value>\d+(?:\.\d+)?)/g);
  for (const match of matches) {
    const index = match.groups?.name.trim()
    if (index) {
      //@ts-ignore
      measures[index] = parseFloat(match.groups.value);
    }
  }
  console.log(measures)
  return measures;
}

/**
 * Get the data from the contentIndex.json file or from session storage
 * @returns {object} The data
 */
async function getData() {
  const key = "nutritionFact";
  let sessionData = sessionStorage.getItem(key);
  if (sessionData) {
    try {
      return JSON.parse(sessionData);
    } catch (error) {
      console.error('Error while parsing data', error);
    }
  }

  const data = {
    ingredients: {},
    measures: {}
  } as any;

  const response = await (await fetch("/static/contentIndex.json")).json();
  const ingredientFolder = "Ingrédients";
  const measures = "mesures";
  for (const key in response) {
    if (key.includes(ingredientFolder)) {
      data.ingredients[key.replace(ingredientFolder + '/', '')] = parseIngredient(response[key].content);
    }
    if (key.includes(measures)) {
      data.measures = parseMeasures(response[key].content);
    }
  }
  sessionStorage.setItem(key, JSON.stringify(data));
  return data;
}

/**
 * Parse the ingredient to extract quantity factor to convert from 100g to the ingredient quantity
 * @param {string} valStr - The ingredient to parse (e.g. 1 kg or 1 œuf)
 * @returns {number} The unit factor (e.g. 1 for 1 kg)
 */
function _parseIngredientQuantity(valStr: string | null): number {
  const match = valStr?.match(/(?<quantity>\d+(?:[,.]\d+)?)/);
  if (!match) {
    return 1;
  }
  return parseFloat(match.groups?.quantity?.replace(',', '.') ?? "1");
}

/**
 * Parse the ingredient to extract quantity factor to convert from 100g to the ingredient quantity
 * @param {string} valStr - The ingredient to parse (e.g. 1 kg or 1 œuf)
 * @param {object} data - The data to use for the parsing
 * @returns {number} The unit factor
 */
function _parseIngredient(valStr: string | null, data: any, removeUselessWords = true) {
  const match = valStr?.match(/(?<quantity>\d+(?:[,.]\d+)?)? ?(?<unit>\D*)$/);
  if (!match || !match.groups?.unit) {
    return null;
  }

  let unit = match.groups.unit.toLocaleLowerCase();
  unit = unit.replace(/([^ ]{3,15})s\b/g, '$1');

  if (removeUselessWords) {
    unit = unit.replace(/\bdu\b|\bde\b|\bd’/g, '');
  }

  unit = unit.replace(/ +/g, ' ').trim();
  if (unit == "") {
    return null;
  }

  const unitFactor = data.measures[unit];
  if (!unitFactor) {
    console.debug(`Unit not found: '${unit}'`);
    return null;
  }
  return unitFactor;
}

/**
 * Find the base unit of the ingredient
 * @param {string} valStr - The ingredient to parse (e.g. 1 kg or 1 cl)
 * @param {object} data - The data to use for the parsing
 */
function findBaseUnit(valStr: string | null, data: any) {
  const match = valStr?.matchAll(/(?<baseUnit>\b[^ ]{1,3}\b)/g);
  if (!match) {
    return null;
  }
  for (const m of match) {
    const baseUnit = m.groups?.baseUnit.toLocaleLowerCase();
    if (baseUnit && data.measures[baseUnit]) {
      return data.measures[baseUnit]
    }
  }
  return null
}

/**
 * Parse the ingredient line to extract the nutrition facts
 * @param {Element} line - The line to parse
 * @param {object} data - The data to use for the parsing
 * @returns {object} The nutrition facts
 */
function parseIngredientLine(line: Element, data: any): { [key: string]: NutritionValue | null } | null {
  const name = decodeURI(line.querySelector("a")?.href.split('/').pop() ?? "");
  let value = getEmptyValues();
  if (name && data.ingredients[name]) {
    // parse whole line
    let texts = [line.textContent, line.childNodes[0]?.textContent] as (string | null)[];
    let unitFactor = null;
    let quantity = _parseIngredientQuantity(line.textContent);

    for (let text of texts) {
      if (!text) continue;
      text = text.replace(quantity + "", '').replace("'", "’").trim();
      // Check if the unit is in the measures
      if (data.measures[text]) {
        unitFactor = data.measures[text];
        break;
      }

      unitFactor = findBaseUnit(text, data);
      if (!unitFactor) {
        unitFactor = _parseIngredient(text, data, false);
      }
      if (!unitFactor) {
        unitFactor = _parseIngredient(text, data, true);
      }
      if (unitFactor) {
        break;
      }
    }

    if (!unitFactor) {
      return null;
    }

    unitFactor = unitFactor * quantity / 100;


    const ingredient = data.ingredients[name];
    for (const key in ingredient) {
      if (ingredient[key]) {
        value[key] = ingredient[key]!;
        value[key]!.quantity *= unitFactor;
      }
    }
  } else {
    console.error(`Ingredient not found: ${name}`);
    return null;
  }
  return value;
}

/**
 * Add two nutrition values together
 * @param {object} a - The first nutrition value
 * @param {object} b - The second nutrition value
 * @returns {object} The sum of the two nutrition values
 */
function addNutritionValue(a: NutritionValue | null, b: NutritionValue | null) {
  if (!a) {
    return b;
  }
  if (!b) {
    return a;
  }
  if (a.unit !== b.unit) {
    console.error(`Unit mismatch: ${a.unit} != ${b.unit}`);
    return a;
  }
  return {
    unit: a.unit,
    quantity: a.quantity + b.quantity
  }
}

/**
 * Find the portion of the recipe
 * @returns {{value: number, text: string}} The portion of the recipe
 */
function findPortion(): { value: number; text: string; } {
  const portions = [...document.querySelectorAll('tr')].find((tr) => {
    const td = tr.querySelector('td');
    if (!td || !td.textContent) return false;
    if (td.textContent.includes('Pour') || td.textContent.includes('Portion')) {
      return true;
    }
  });
  const text = portions?.querySelector('td:nth-of-type(2)')?.textContent;
  if (!text) {
    return { value: 1, text: "Toute la recette" };
  }
  const match = text.match(/^ ?(?<value>[\d.,]+)(?<text>.*)$/);
  if (!match || !match.groups?.value) {
    return { value: 1, text: "toute la recette" };
  }

  return { value: parseFloat(match.groups.value.replace(/\,/g, '.')), text: '1 ' + match.groups.text?.trim()?.replace(/s\b/g, '')};
}

/**
 * Calculate the nutrition facts of the recipe
 * and display them in the nutrition fact table
 */
async function calculateNutritionFact() {
  document.getElementById('nutrition-facts')?.classList.remove('hidden');
  let errors = [];
  const data = await getData();
  const ingredients = [...document.querySelectorAll('#ingrédients ~ ul li')];
  const nutritionFacts = getEmptyValues();
  for (let ingredient of ingredients) {
    if (!ingredient.querySelector("a")) {
      continue;
    }
    const currentFact = parseIngredientLine(ingredient, data);
    if (!currentFact) {
      errors.push(ingredient.textContent);
      console.error(`Error while parsing ingredient: ${ingredient.textContent}`)
      continue;
    }
    for (const key in currentFact) {
      nutritionFacts[key] = addNutritionValue(nutritionFacts[key], currentFact[key]);
    }
  }
  const portion = findPortion();
  document.querySelector('#portion')!.innerHTML = `Pour ${portion.text}`;
  for (const key in nutritionFacts) {
    if (nutritionFacts[key]) {
      const quantity = nutritionFacts[key]!.quantity / portion.value;
      appendLine(key, `${quantity.toFixed(1)} ${nutritionFacts[key]!.unit}`);
    }
  }
  if (errors.length > 0) {
    const _c = (type: string) => (e: any) => {
      const a = document.createElement(type);
      a.innerHTML = e;
      return a;
    };
    const nf = document.querySelector('#nutrition-facts');
    nf?.appendChild(_c('div')('⚠️ Non pris en compte'));
    const ul = document.createElement('ul');
    errors.map(_c('li')).forEach((e) => ul.appendChild(e));
    nf?.appendChild(ul);
  }
}

/**
 * Append a new line to the nutrition fact table
 * @param {string} name - The name of the nutrition fact
 * @param {string} value - The value of the nutrition fact
 */
function appendLine(name: string, value: string) {
  const tr = document.createElement('tr');
  tr.innerHTML = `<td><b>${name}<b></td><td>${value}</td>`;
  document.querySelector('#nutrition-facts tbody')?.appendChild(tr);
}

document.addEventListener("nav", () => {
  if (!isRecipePage()) {
    deleteNutritionFact();
  } else {
    calculateNutritionFact();
  }
});