import { FullSlug, resolveRelative } from "../util/path"
import { QuartzPluginData } from "../plugins/vfile"
import { Date, getDate } from "./Date"
import { QuartzComponent, QuartzComponentProps } from "./types"
import { GlobalConfiguration } from "../cfg"
import pageListStyle from "./styles/pageList.scss"

export function byDateAndAlphabetical(
  cfg: GlobalConfiguration,
): (f1: QuartzPluginData, f2: QuartzPluginData) => number {
  return (f1, f2) => {
    if (f1.dates && f2.dates) {
      // sort descending
      return getDate(cfg, f2)!.getTime() - getDate(cfg, f1)!.getTime()
    } else if (f1.dates && !f2.dates) {
      // prioritize files with dates
      return -1
    } else if (!f1.dates && f2.dates) {
      return 1
    }

    // otherwise, sort lexographically by title
    const f1Title = f1.frontmatter?.title.toLowerCase() ?? ""
    const f2Title = f2.frontmatter?.title.toLowerCase() ?? ""
    return f1Title.localeCompare(f2Title)
  }
}

type Props = {
  limit?: number
} & QuartzComponentProps

export const PageList: QuartzComponent = ({ cfg, fileData, allFiles, limit }: Props) => {
  let list = allFiles.sort(byDateAndAlphabetical(cfg))
  if (limit) {
    list = list.slice(0, limit)
  }

  return (
    <div class="list">
      {list.filter(a => !(a.frontmatter?.ignoreInList ?? false)).map((page) => {
        const title = page.frontmatter?.title
        //@ts-ignore
        const image = (page.frontmatter?.image ?? [])[0]
        //@ts-ignore
        const tags = page.frontmatter?.tags ?? []

        return (
          <article>

            <div>
              <div className="image">
                {image && (
                  <figure>
                    <a href={resolveRelative(fileData.slug!, page.slug!)}>
                      <img src={image} alt="Visuel de la recette. non contractuel" />
                    </a>
                  </figure>
                )}
              </div>
              <div className="title">
                <a href={resolveRelative(fileData.slug!, page.slug!)}>
                  <h3>{title}</h3>
                </a>
              </div>
              <div className="info">
                {tags.map((tag) => (
                  <a
                    class="internal tag-link"
                    href={resolveRelative(fileData.slug!, `tags/${tag}` as FullSlug)}
                  >
                    {tag}
                  </a>
                ))}
              </div>
            </div>
          </article>
        )
      })}
    </div>
  )
}

PageList.css = pageListStyle;
