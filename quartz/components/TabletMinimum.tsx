import { QuartzComponent, QuartzComponentConstructor, QuartzComponentProps } from "./types"

export default ((component?: QuartzComponent) => {
  if (component) {
    const Component = component
    const TabletMinimum: QuartzComponent = (props: QuartzComponentProps) => {
      return <Component displayClass="min-tablet" {...props} />
    }

    TabletMinimum.displayName = component.displayName
    TabletMinimum.afterDOMLoaded = component?.afterDOMLoaded
    TabletMinimum.beforeDOMLoaded = component?.beforeDOMLoaded
    TabletMinimum.css = component?.css
    return TabletMinimum
  } else {
    return () => <></>
  }
}) satisfies QuartzComponentConstructor
